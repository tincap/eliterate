@extends('layouts.master')

@section('content')
    <section class="page-content company">
        <div class="container">
            <div class="page-head">
                <div class="page-head__name">
                    <h1>Просмотр кампании</h1>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Главная</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('campaigns.index') }}">Видео-кампании</a></li>
                            <li class="breadcrumb-item active" aria-current="page">
                                <a href="{{ route('campaigns.show', ['slug' => $campaign->slug]) }}">Просмотр кампании</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="company-content">
                <div class="row">
                    <div class="col-md-7">
                        <img src="{{ asset($campaign->imageUrl) }}" alt="">
                    </div>
                    <div class="col-md-5">
                        <div class="companies-col__content">
                            <h3 class="companies-col__title">{{ $campaign->name }}</h3>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
                                    {{ $campaign->progress }}%
                                </div>
                            </div>
                            <div class="companies-col__info">
                                <p><span>Стоимость просмотра:</span><b>до {{ $campaign->price }} RUB</b></p>
                                <p><span>Доступный бюджет:</span><b>{{ $campaign->available_budget }} RUB</b></p>
                                <p><span>Доступно:</span><b>{{ count($platforms) }} {{ trans_choice('campaigns::campaigns.platforms', count($platforms)) }}</b></p>
                                <p><span>Дата старта:</span><b>{{ $campaign->start_at->format('d.m.Y, h:m') }}</b></p>
                            </div>
                            <div class="companies-col__bottom">
                                <p>Условия размещения</p>
                                <ul>
                                    @foreach($campaign->conditions as $condition)
                                        <li>— {{ $condition->name }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="company-table">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-4">
                                <p>Площадка</p>
                            </div>
                            <div class="col-2">
                                <p>Стоимость</p>
                            </div>
                            <div class="col-4">
                                <p>последнее размещение</p>
                            </div>
                            <div class="col-2">
                                <p>таймер размещений</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body card-playgrounds">
                        @include('campaigns::platform_list')
                        <div class="card-playgrounds__btn">
                            <a class="btn btn-success" href="#">Разместить</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop