@foreach($platforms as $platform)
    <div class="row">
        <div class="form-check">
            <input class="form-check-input" type="checkbox" id="blankCheckbox" value="option1" aria-label="...">
        </div>
        <div class="col-4">
            <div class="card-col__row">
                <div class="card-col__img">
                    <img src="{{ asset('img/human-small.png') }}" alt="">
                </div>
                <div>
                    <p>{{ $platform->name }}</p>
                </div>
            </div>
        </div>
        <div class="col-2">
            <p>{{ $platform->price }} RUB</p>
        </div>
        <div class="col-4">
            <p>Не размещена</p>
        </div>
        <div class="col-2">
            <span class="btn-plus">+</span>
        </div>
    </div>
@endforeach