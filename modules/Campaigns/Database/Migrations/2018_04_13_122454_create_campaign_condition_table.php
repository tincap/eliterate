<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignConditionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_condition', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('campaign_id');
            $table->unsignedInteger('condition_id');

            $table->foreign('campaign_id')
                ->references('id')
                ->on('campaigns');

            $table->foreign('condition_id')
                ->references('id')
                ->on('conditions');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campaign_condition', function (Blueprint $table) {
            $table->dropForeign('campaign_condition_campaign_id_foreign');
            $table->dropForeign('campaign_condition_condition_id_foreign');
        });

        Schema::dropIfExists('campaign_condition');
    }
}
