<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            // data
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->string('video');
            $table->string('image')->default('img/default/campaign.jpg');
            $table->text('desc');

            $table->unsignedInteger('views')->default(0);

            $table->decimal('price')->default(0)
                ->comment('Цена за просмотр');

            $table->decimal('available_budget')->default(0)
                ->comment('Осталось денег');

            $table->decimal('budget')->default(0)
                ->comment('Всего доступно денег');

            $table->boolean('active')->default(true);
            $table->unsignedInteger('order')->default(1000);
            // timestamps
            $table->timestamp('start_at')->default(now());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns');
    }
}
