<?php

Route::group(['middleware' => ['web', 'auth'], 'namespace' => 'Modules\Campaigns\Http\Controllers'], function()
{
    Route::group(['prefix' => '/campaigns', 'as' => 'campaigns.'], function () {
        Route::get('/', 'CampaignsController@index')->name('index');
        Route::post('/', 'CampaignsController@search');
        Route::get('/{campaign}', 'CampaignsController@show')->name('show');
    });
});
