@extends('admin::layouts.master')

@section('title', 'Countries')

@section('content')
    <div id="main" role="main">

        <!-- RIBBON -->
        <div id="ribbon">

            <!-- breadcrumb -->
            <ol class="breadcrumb">
                <li>Главная</li>
                <li>{{ $country->id ? 'Редактирование' : 'Добавление' }} страны</li>
            </ol>

        </div>
        <!-- END RIBBON -->

        <!-- #MAIN CONTENT -->

        <div id="content">

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h1 class="page-title txt-color-blueDark">

                        <!-- PAGE HEADER -->
                        <i class="fa-fw fa fa-user"></i>
                        {{ $country->id ? 'Редактирование' : 'Добавление' }} страны
                    </h1>
                </div>

            </div>

            @if(session('success', false))
                <div class="alert alert-block alert-success">
                    <a class="close" data-dismiss="alert" href="#">×</a>
                    <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Check validation!</h4>
                    <p>
                        You may also check the form validation by clicking on the form action button. Please try and see the results below!
                    </p>
                </div>
            @endif

            <div class="row">

                <div class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

                    <div class="jarviswidget well" >

                        <!-- widget content -->
                        <div class="widget-body">

                            <form method="POST" class="form-horizontal" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <fieldset>
                                    <legend>{{ $country->id ? 'Редактирование' : 'Добавление' }} данных</legend>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Название страны</label>

                                        <div class="col-md-10">
                                            <input value="{{ $country->name }}" name="name" class="form-control" placeholder="Название страны" type="text">
                                        </div>
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="select-1">Валюта</label>

                                        <div class="col-md-10">
                                            <input value="{{ $country->currency }}" name="currency" placeholder="Валюта" class="form-control" type="text">
                                            @if ($errors->has('currency'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('currency') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="select-1">Код</label>

                                        <div class="col-md-10">
                                            <input value="{{ $country->code }}" name="code" placeholder="Код" class="form-control" type="text">
                                            @if ($errors->has('code'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('code') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Загрузить флаг</label>
                                        <div class="col-md-10">
                                            <input name="img" id="exampleInputFile1" type="file">
                                            @if ($errors->has('img'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('img') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                </fieldset>

                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button class="btn btn-primary" type="submit">
                                                <i class="fa fa-save"></i>
                                                Сохранить
                                            </button>
                                        </div>
                                    </div>
                                </div>

                            </form>

                        </div>
                        <!-- end widget content -->
                    </div>

                </div>
            </div>
        </div>

        <!-- END #MAIN CONTENT -->

    </div>
@stop