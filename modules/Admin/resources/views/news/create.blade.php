@extends('admin::layouts.master')

@section('title', 'Создание новости')

@section('content')
    <div id="main" role="main">

        <!-- RIBBON -->
        <div id="ribbon">

            <!-- breadcrumb -->
            <ol class="breadcrumb">
                <li>Главная</li>
                <li>Создание новости</li>
            </ol>

        </div>
        <!-- END RIBBON -->

        <!-- #MAIN CONTENT -->

        <!-- col -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark">

                <!-- PAGE HEADER -->
                <i class="fa-fw fa fa-rss"></i>
                Создание новости
            </h1>
        </div>
        <!-- end col -->

        <div class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

            <!--<div class="alert alert-block alert-success">
                <a class="close" data-dismiss="alert" href="#">×</a>
                <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Check validation!</h4>
                <p>
                    You may also check the form validation by clicking on the form action button. Please try and see the results below!
                </p>
            </div>-->

            @include('admin::news._form', ['article' => $article])

        </div>

        <!-- end row -->

        <!-- END #MAIN CONTENT -->

    </div>
@stop
