<nav>
    <ul>
        <li class="">
            <a href="{{ route('admin.offers.create') }}"><i class="fa fa-plus"></i>Добавление оффера</a>
        </li>

        <li class="treeview">
            <a href="#">
                <i class="fa fa-rss"></i>
                <span>Офферы</span>
                <b class="collapse-sign"><em class="fa fa-plus-square-o"></em></b></a>
            <ul class="treeview-menu" style="display: none;">
                <li><a href="{{ route('admin.offers.list') }}">Офферы</a></li>
                <li><a href="{{ route('admin.offers.categories.index') }}">Категории оффера</a></li>
                <li><a href="{{ route('admin.offers.categories.create') }}">Создание категории оффера</a></li>
            </ul>
        </li>

        <li class="treeview">
            <a href="#">
                <i class="fa fa-rss"></i>
                <span>Новости</span>
                <b class="collapse-sign"><em class="fa fa-plus-square-o"></em></b></a>
            <ul class="treeview-menu" style="display: none;">
                <li><a href="{{ route('admin.news.index') }}">Список новостей</a></li>
                <li><a href="{{ route('admin.news.create') }}">Создание новости</a></li>
            </ul>
        </li>

        <li class="treeview">
            <a href="#">
                <i class="fa fa-ticket"></i>
                <span>Тикеты</span>
                <b class="collapse-sign"><em class="fa fa-plus-square-o"></em></b></a>
            <ul class="treeview-menu" style="display: none;">
                <li><a href="{{ route('admin.tickets.index') }}">Список тикетов</a></li>
                {{--<li><a href="{{ route('admin.tickets.index') }}">Тикеты пользователя</a></li>--}}
                <li><a href="{{ route('admin.tickets.create') }}">Создание тикета</a></li>
                {{--<li><a href="{{ route('admin.tickets.chat', ['id' => '1']) }}">Чат тикета</a></li>--}}
            </ul>
        </li>

        <li class="treeview">
            <a href="#">
                <i class="fa fa-user"></i>
                <span>Пользователь</span>
                <b class="collapse-sign"><em class="fa fa-plus-square-o"></em></b></a>
            <ul class="treeview-menu" style="display: none;">
                <li><a href="{{ route('admin.users.index') }}">Список пользователей</a></li>
{{--                <li><a href="{{ route('admin.users.statistics') }}">Статистика пользователя</a></li>--}}
                <li><a href="{{ route('admin.users.platforms') }}">Площадки пользователя</a></li>
                <li><a href="{{ route('admin.wallets.index') }}">Кошельки пользователей</a></li>
            </ul>
        </li>

        <li class="treeview">
            <a href="#">
                <i class="fa fa-rss"></i>
                <span>Страны</span>
                <b class="collapse-sign"><em class="fa fa-plus-square-o"></em></b></a>
            <ul class="treeview-menu" style="display: none;">
                <li><a href="{{ route('admin.countries.index') }}">Список стран</a></li>
                <li><a href="{{ route('admin.countries.create') }}">Добавление страны</a></li>
            </ul>
        </li>

        <li class="treeview">
            <a href="#">
                <i class="fa fa-rss"></i>
                <span>Видео кампании</span>
                <b class="collapse-sign"><em class="fa fa-plus-square-o"></em></b></a>
            <ul class="treeview-menu" style="display: none;">
                <li><a href="{{ route('admin.campaigns.index') }}">Список кампаний</a></li>
                <li><a href="{{ route('admin.campaigns.create') }}">Добавление кампании</a></li>
            </ul>
        </li>

    </ul>
</nav>