@foreach($categories as $category)
    <tr>
        <td>{{ $category->id }}</td>
        <td><a href="{{ route('admin.offers.categories.edit', ['id' => $category->id]) }}">{{ $category->name }}</a>
        </td>
        <td><a href="{{ route('admin.offers.categories.edit', ['id' => $category->id]) }}">{{ $category->slug }}</a>
        </td>
        <td>{{ $category->isActive ? 'активен' : 'не активен' }}</td>
        <td>{{ $category->order }}</td>
        <td>
            <a href="{{ route('admin.offers.categories.edit', ['id' => $category->id]) }}" class="btn btn-info btn-sm">Редактировать</a>
            <a href="{{ route('admin.offers.categories.destroy', ['id' => $category->id]) }}" class="btn btn-danger btn-sm btn-delete">Удалить</a>
        </td>
    </tr>
@endforeach