@extends('admin::layouts.master')

@section('title', 'Index')

@section('content')
    <div id="main" role="main">

        <!-- RIBBON -->
        <div id="ribbon">

            <!-- breadcrumb -->
            <ol class="breadcrumb">
                <li>Главная</li>
                <li>{{ $category->id ? 'Редактирование' : 'Создание' }} категории</li>
            </ol>

        </div>
        <!-- END RIBBON -->

        <!-- #MAIN CONTENT -->

        <div id="content">

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h1 class="page-title txt-color-blueDark">

                        <!-- PAGE HEADER -->
                        <i class="fa-fw fa fa-user"></i>
                        {{ $category->id ? 'Редактирование' : 'Создание' }} категории
                    </h1>
                </div>

            </div>

            {{--<div class="alert alert-block alert-success">--}}
            {{--<a class="close" data-dismiss="alert" href="#">×</a>--}}
            {{--<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Check validation!</h4>--}}
            {{--<p>--}}
            {{--You may also check the form validation by clicking on the form action button. Please try and see the results below!--}}
            {{--</p>--}}
            {{--</div>--}}

            <div class="row">

                <div class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

                    <div class="jarviswidget well" >

                        <!-- widget content -->
                        <div class="widget-body">

                            <form method="POST" class="form-horizontal">
                                {{ csrf_field() }}
                                <fieldset>
                                    <legend>{{ $category->id ? 'Редактирование' : 'Создание' }} данных</legend>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Название категории</label>

                                        <div class="col-md-10">
                                            <input value="{{ $category->name }}" name="name" class="form-control" placeholder="Название категории" type="text">
                                        </div>
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="select-1">Псевдоним категории</label>

                                        <div class="col-md-10">
                                            <input value="{{ $category->slug }}" name="slug" placeholder="Псевдоним категории" class="form-control" type="text">
                                            @if ($errors->has('slug'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('slug') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="select-1">Приоритет категории</label>

                                        <div class="col-md-10">
                                            <input value="{{ $category->order }}" name="order" placeholder="Приоритет категории" class="form-control" type="number">
                                            @if ($errors->has('order'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('order') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="select-1">Статус</label>

                                        <div class="col-lg-3">
                                            <label class="checkbox">
                                                <input {{ $category->isActive ? 'checked' : '' }} name="isActive" type="checkbox">
                                            </label>
                                        </div>
                                    </div>

                                </fieldset>

                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button class="btn btn-primary" type="submit">
                                                <i class="fa fa-save"></i>
                                                Сохранить
                                            </button>
                                        </div>
                                    </div>
                                </div>

                            </form>

                        </div>
                        <!-- end widget content -->
                    </div>

                </div>
            </div>
        </div>

        <!-- END #MAIN CONTENT -->

    </div>
@stop