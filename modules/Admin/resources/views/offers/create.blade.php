@extends('admin::layouts.master')

@section('content')

    <!-- #MAIN PANEL -->
    <div id="main" role="main">

        <!-- RIBBON -->
        <div id="ribbon">

            <!-- breadcrumb -->
            <ol class="breadcrumb">
                <li>Главная</li>
                <li>Добавление оффера</li>
            </ol>

        </div>
        <!-- END RIBBON -->

        <!-- #MAIN CONTENT -->

        <!-- col -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark">

                <!-- PAGE HEADER -->
                <i class="fa-fw fa fa-plus"></i>
                Добавление оффера
            </h1>
        </div>
        <!-- end col -->

        <div class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

            <div class="alert alert-block alert-success">
                <a class="close" data-dismiss="alert" href="#">×</a>
                <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Check validation!</h4>
                <p>
                    You may also check the form validation by clicking on the form action button. Please try and see the
                    results below!
                </p>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $item => $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="jarviswidget well">


                <div role="content">


                    <!-- widget content -->
                    <div class="widget-body">

                        <form class="form-horizontal" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <fieldset>
                                <legend>Введите основные данные</legend>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Заголовок</label>
                                    <div class="col-md-10">
                                        <input class="form-control" name="name" placeholder="Название оффера"
                                               value="{{ old('name') }}" type="text">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="select-1">Цель</label>
                                    <div class="col-md-10">

                                        <select class="form-control" name="target" id="select-1">
                                            <option>Подтвержденная заявка</option>
                                            <option>Регитсрация</option>
                                            <option>Депозит</option>
                                        </select>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="select-2">Категория</label>
                                    <div class="col-md-10">

                                        <select class="form-control" name="category_id" required id="select-2">
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Ссылка</label>
                                    <div class="col-md-10">
                                        <input class="form-control" name="link" placeholder="Ссылка на просмотр оффера"
                                               value="{{ old('link') }}" type="text">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Описание</label>
                                    <div class="col-md-10">
                                        <textarea class="form-control" name="description" placeholder=""
                                                  rows="4">{{ old('description') }}</textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Правила</label>
                                    <div class="col-md-10">
                                        <textarea class="form-control" name="rules" placeholder="Необязательно"
                                                  rows="4">{{ old('rules') }}</textarea>
                                    </div>
                                </div>


                            </fieldset>

                            <fieldset>
                                <legend>Выберите постер оффера</legend>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Постер</label>
                                    <div class="col-md-10">
                                        <input id="exampleInputFile1" name="image" type="file"
                                               value="{{ old('image') }}">
                                    </div>
                                </div>

                            </fieldset>

                            <fieldset>
                                <legend>Выберите партнёрскую программу</legend>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Программа</label>
                                    <div class="col-xs-9 col-md-6">
                                        <select name="partner_type" style="width: 100%;height: 32px">
                                            <option value="">Не выбрано</option>
                                            @foreach($partners as $name => $program)
                                                <option value="{{ $name }}">
                                                    {{ $program::getName() }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Ссылка или id</label>
                                    <div class="col-xs-9 col-md-6">
                                        <input name="partner_link" type="text" class="form-control"
                                               value="{{ old('partner_link') }}">
                                    </div>
                                </div>

                            </fieldset>

                            <fieldset>
                                <legend>Лендинги</legend>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Доступные лендинги</label>
                                    <div class="col-md-9">
                                        <input name="has_landing" type="checkbox">

                                        <button type="button" class="btn btn-sm btn-info hidden" id="addLand">
                                            <i class="fa fa fa-plus-circle"></i>
                                        </button>
                                    </div>
                                </div>

                                <div class="land-list hidden">

                                    <div class="form-group hidden">
                                        <label class="col-sm-2 control-label">
                                            <span class="country">Лендинг</span>
                                        </label>

                                        <div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon">Название:</span>
                                                <input class="form-control" name="landing_name" type="text">
                                            </div>
                                        </div>

                                        <div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon">Ссылка:</span>
                                                <input class="form-control" name="url" type="text">
                                            </div>
                                        </div>

                                        <div class="col-sm-1">
                                            <button type="button" class="btn btn-danger btn-sm btn-remove">
                                                <i class="fa fa-times-circle"></i>
                                            </button>
                                        </div>
                                    </div>

                                </div>

                            </fieldset>


                            <fieldset>
                                <legend>Редактирование ставок</legend>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Выберите страну</label>

                                    <div class="col-xs-9 col-md-6">

                                        {{--<span class="select2 select2-container select2-container--default select2-container--below select2-container--focus"--}}
                                        {{--dir="ltr" style="width: 100%;">--}}
                                        {{--<span class="selection">--}}
                                        {{--<span class="select2-selection select2-selection--single"--}}
                                        {{--role="combobox" aria-haspopup="true" aria-expanded="false"--}}
                                        {{--tabindex="0" aria-labelledby="select2-4yww-container">--}}
                                        {{--<span class="select2-selection__rendered"--}}
                                        {{--id="select2-4yww-container" title="Washington">--}}

                                        {{--</span>--}}
                                        {{--<span class="select2-selection__arrow" role="presentation">--}}
                                        {{--<b role="presentation"></b>--}}
                                        {{--</span>--}}
                                        {{--</span>--}}
                                        {{--</span>--}}

                                        {{--<span class="dropdown-wrapper" aria-hidden="true"></span>--}}
                                        {{--</span>--}}

                                        <select id="countrySelector" style="width: 100%;height: 32px">
                                            @foreach($countries as $country)
                                                <option value="{{ $country->id }}" data-name="{{ $country->name }}"
                                                        data-currency="{{ $country->currency }}"
                                                        data-image="{{ asset($country->img) }}"
                                                >{{ $country->name }}</option>
                                            @endforeach
                                        </select>

                                    </div>

                                    <div class="col-sm-1">
                                        <button type="button" class="btn btn-info btn-sm" id="add-geo-button">
                                            <i class="fa fa-plus-circle"></i>
                                        </button>
                                    </div>

                                </div>


                                <div class="rate-list">

                                    <div class="form-group hidden">
                                        <label class="col-sm-2 control-label">
                                            <span><img src="" class="flags" alt=""></span>
                                            <span class="country"></span>
                                            <input class="form-control" name="country_id" type="hidden">
                                        </label>

                                        <div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon">Ставка:</span>
                                                <input class="form-control" name="rate" type="text">
                                                <span class="input-group-addon"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon">Цена товара:</span>
                                                <input class="form-control" name="price" type="text">
                                                <span class="input-group-addon"></span>
                                            </div>
                                        </div>

                                        <div class="col-sm-1">
                                            <button type="button" class="btn btn-danger btn-sm btn-remove">
                                                <i class="fa fa-times-circle"></i>
                                            </button>
                                        </div>
                                    </div>

                                </div>

                            </fieldset>

                            <input type="hidden" name="rates">
                            <input type="hidden" name="landings">

                            <fieldset>
                                <legend>Включите разрешенные виды трафика</legend>

                                <div class="form-group">

                                    <label class="col-xs-2"></label>

                                    <div class="col-lg-3">
                                        @foreach($traffics as $traffic)
                                            <label class="checkbox">
                                                <input name="traffic[]" value="{{ $traffic->id }}" type="checkbox">
                                                <i></i>{{ $traffic->name }}</label>
                                        @endforeach
                                    </div>

                                </div>

                            </fieldset>

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12">
                                        <button class="btn btn-primary" type="submit">
                                            <i class="fa fa-save"></i>
                                            Сохранить
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div>
                    <!-- end widget content -->

                </div>

            </div>

        </div>

        <!-- end row -->

        <!-- END #MAIN CONTENT -->

    </div>
    <!-- END #MAIN PANEL -->

    <!--================================================== -->
    <style>
        .flags {
            max-width: 17px;
        }
    </style>
    <!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)
    <script data-pace-options='{ "restartOnRequestAfter": true }' src="js/plugin/pace/pace.min.js"></script>-->
@stop

@section('js')

    <!-- IMPORTANT: APP CONFIG -->
    <script src="{{ asset('modules/admin/js/app.config.js') }}"></script>
    <script src="{{ asset('modules/admin/js/bootstrap/bootstrap.min.js') }}"></script>
    <script src="{{ asset('modules/admin/js/plugin/select2/select2.min.js') }}"></script>

    <script type="text/javascript">

        function calcGeo() {
            var rates = [];

            $('.rate-list .form-group').each(function () {
                if (!$(this).hasClass('hidden')) {
                    rates.push({
                        country_id: parseFloat($(this).find('input[name="country_id"]').val()),
                        rate: parseFloat($(this).find('input[name="rate"]').val()),
                        price: parseFloat($(this).find('input[name="price"]').val()),
                    })
                }
            })

            $('input[name="rates"]').val(JSON.stringify(rates))
        }

        $('#add-geo-button').click(function () {
            var $country_selector = $('#countrySelector');
            var $country = $('#countrySelector').find('option[value="' + $country_selector.val() + '"]')
            var $block = $('.rate-list').children().first().clone();

            $block.find('.country').html($country.attr('data-name'))

            $block.find('.flags').attr('src', $country.attr('data-image'));
            $block.find('input[name="country_id"]').parent().children().last().val($country.attr('value'))
            $block.find('input[name="rate"]').parent().children().last().html($country.attr('data-currency'))
            $block.find('input[name="price"]').parent().children().last().html($country.attr('data-currency'))

            $('.rate-list').append($block.removeClass('hidden'));
        });


        $('.rate-list').delegate('.btn-remove', 'click', function () {
            $(this).parent().parent().find('input').first().trigger('input')
            calcGeo()
        })

        $('.rate-list').delegate('input', 'input', function () {
            calcGeo()
        })


        //END OF RATE LIST

        function calcLands() {
            var lands = [];

            $('.land-list .form-group').each(function () {
                if (!$(this).hasClass('hidden')) {
                    lands.push({
                        name: $(this).find('input[name="landing_name"]').val(),
                        url: $(this).find('input[name="url"]').val(),
                    })
                }
            })

            $('input[name="landings"]').val(JSON.stringify(lands))
        }

        function updateLandings() {
            if ($('input[name="has_landing"]').is(':checked')) {
                $('#addLand').removeClass('hidden')
                $('.land-list').removeClass('hidden')
            } else {
                $('#addLand').addClass('hidden')
                $('.land-list').addClass('hidden')
            }
        }

        $('input[name="has_landing"]').on('change', function () {
            updateLandings()
        })

        $('#addLand').on('click', function () {
            var $block = $('.land-list').children().first().clone();

            $block.removeClass('hidden');
            $('.land-list').append($block)

        })

        $('.land-list').delegate('input', 'input', function () {
            calcLands()
        })


        $('.land-list').delegate('.btn-remove', 'click', function () {
            $(this).parent().parent().remove()
            calcLands()
        })

        //END OF LANDS

        $('form').on('submit', function (e) {
            if ($('input[name="has_landing"]').is(':checked')) {
                var lands = false;

                $('.land-list .form-group').each(function () {
                    if (!$(this).hasClass('hidden')) {
                        if ($(this).find('input[name="landing_name"]').val() && $(this).find('input[name="url"]').val()) {
                            lands = true;
                        }
                    }
                })

                if (!lands) {
                    alert('Добавьте как минимум 1 лендинг');
                    e.preventDefault()
                }
            }
        })
    </script>
@stop