@extends('admin::layouts.master')

@section('title', 'Creatives')

@section('content')
    <div id="main" role="main">

        <!-- RIBBON -->
        <div id="ribbon">

            <!-- breadcrumb -->
            <ol class="breadcrumb">
                <li>Главная</li>
                <li>{{ $creative->id ? 'Редактирование' : 'Создание' }} креатива</li>
            </ol>

        </div>
        <!-- END RIBBON -->

        <!-- #MAIN CONTENT -->

        <div id="content">

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h1 class="page-title txt-color-blueDark">

                        <!-- PAGE HEADER -->
                        <i class="fa-fw fa fa-user"></i>
                        {{ $creative->id ? 'Редактирование' : 'Создание' }} креатива
                    </h1>
                </div>
            </div>

            @if(session('success', false) === true)
                <div class="alert alert-block alert-success">
                    <a class="close" data-dismiss="alert" href="#">×</a>
                    <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Check validation!</h4>
                    <p>
                        You may also check the form validation by clicking on the form action button. Please try and see
                        the results below!
                    </p>
                </div>
            @endif

            <div class="row">

                <div class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

                    <div class="jarviswidget well">

                        <!-- widget content -->
                        <div class="widget-body">

                            <form method="POST" class="form-horizontal">
                                {{ csrf_field() }}
                                <input id="images" value="{{ json_encode($creative->imagesUrl) }}" type="hidden"
                                       name="images">

                                <fieldset>
                                    <legend>{{ $creative->id ? 'Редактирование' : 'Создание' }} данных</legend>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Название креатива</label>

                                        <div class="col-md-10">
                                            <input value="{{ $creative->name }}" name="name" class="form-control"
                                                   placeholder="Название креатива" type="text">
                                        </div>
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Текст креатива</label>

                                        <div class="col-md-10">
                                            <textarea rows="4" name="text" class="form-control"
                                                      placeholder="Текст">{{ $creative->text }}</textarea>
                                        </div>
                                        @if ($errors->has('text'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('text') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                </fieldset>

                                <div class="container-fluid" id="creatives">
                                    <div class="preview"></div>
                                </div>

                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button class="btn btn-primary" type="submit">
                                                <i class="fa fa-save"></i>
                                                Сохранить
                                            </button>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                        <!-- end widget content -->
                    </div>

                </div>
            </div>
        </div>

        <div id="template-creatives" hidden>
            <div class="dz-preview dz-file-preview col-md-2" style="margin: 20px 0;width: 226px;">
                <div class="dz-details">
                    <img data-dz-thumbnail/>
                </div>
                <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
                <div class="dz-error-message"><span data-dz-errormessage></span></div>
            </div>
        </div>

        <!-- END #MAIN CONTENT -->

    </div>

    <style>
        .well .form-actions {
            margin-top: 25px;
            margin-left: -13px;
            margin-right: -13px;
            margin-bottom: -13px;
        }

        #creatives {
            min-height: 200px;
            border: 1px dashed silver;
            background: url('/img/cloud.png') no-repeat center;
        }

        .dz-remove {
            width: 100%;
            display: inline-block;
            text-align: center;
        }
    </style>

    @push('js')
        <script type="text/javascript">
            $(function () {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                var creatives = new Dropzone("#creatives", {
                    url: {!! json_encode(route('image_upload')) !!},
                    previewTemplate: document.querySelector('#template-creatives').innerHTML,
                    previewsContainer: document.querySelector('.preview').innerHTML,
                    parallelUploads: 5,
                    maxFiles: 5,
                    paramName: "upload",
                    addRemoveLinks: true,
                    thumbnailWidth: 200,
                    thumbnailHeight: 200,

                    init: function () {
                        var images = JSON.parse($('#images').attr('value'));

                        var _self = this;

                        if (images) {
                            $.each(images, function (k, v) {
                                var mockFile = {
                                    dataURL: (document.location.origin + v),
                                    filename: v.split('/').pop()
                                };

                                _self.emit("addedfile", mockFile);

                                _self.createThumbnailFromUrl(mockFile, 200, 200, "crop", true, function (thumbnail) {
                                    _self.emit('thumbnail', mockFile, thumbnail);
                                });

                                _self.emit("complete", mockFile);

                                _self.files.push(mockFile);

                                _self._updateMaxFilesReachedClass();
                            });
                        }

                        this.on("removedfile", function (file) {
                            if (!file.filename) {
                                file.filename = JSON.parse(file.xhr.response).filename;
                            }

                            $.ajax({
                                type: "POST",
                                url: "{!! route('admin.creatives.removeUpload') !!}",
                                data: {
                                    file: file.filename,
                                    id: "{!! $creative->id ?? 0 !!}"
                                }
                            });
                        });
                    }
                });

                $('.form-horizontal').submit(function () {
                    var arr = creatives.files.map(function (current) {
                        var response = current.xhr ? JSON.parse(current.xhr.response) : false;

                        if (response.uploaded) {
                            return response.filename;
                        }

                        return current.filename;
                    });

                    $('#images').attr('value', JSON.stringify(arr));
                });
            });
        </script>
    @endpush
@stop