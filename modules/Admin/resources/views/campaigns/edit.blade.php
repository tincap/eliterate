@extends('admin::layouts.master')

@section('title', 'Campaigns')

@section('content')
    <div id="main" role="main">

        <!-- RIBBON -->
        <div id="ribbon">

            <!-- breadcrumb -->
            <ol class="breadcrumb">
                <li>Главная</li>
                <li>{{ $campaign->id ? 'Редактирование' : 'Добавление' }} видео кампании</li>
            </ol>

        </div>
        <!-- END RIBBON -->

        <!-- #MAIN CONTENT -->

        <!-- col -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark">

                <!-- PAGE HEADER -->
                <i class="fa-fw fa fa-plus"></i>
                {{ $campaign->id ? 'Редактирование' : 'Добавление' }} видео кампании
            </h1>
        </div>
        <!-- end col -->

        <div class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

            @if(session('success', false))
                <div class="alert alert-block alert-success">
                    <a class="close" data-dismiss="alert" href="#">×</a>
                    <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Check validation!</h4>
                    <p>
                        You may also check the form validation by clicking on the form action button. Please try and see the results below!
                    </p>
                </div>
            @endif

            <div class="jarviswidget well" >


                <div role="content">


                    <!-- widget content -->
                    <div class="widget-body">

                        <form method="POST" class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <fieldset>
                                <legend>Введите основные данные</legend>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Название кампании</label>
                                    <div class="col-md-10">
                                        <input value="{{ old('name') ?? $campaign->name }}" name="name" class="form-control" placeholder="Название кампании" type="text">
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="select-1">Кол-во просмотров</label>
                                    <div class="col-md-4">
                                        <input value="{{ old('views') ?? $campaign->views }}" name="views" class="form-control" placeholder="Сколько нужно просмотров рекламодателю" type="number">
                                        @if ($errors->has('views'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('views') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Ссылка на видео</label>
                                    <div class="col-md-10">
                                        <input value="{{ old('video') ?? $campaign->video }}" name="video" class="form-control" placeholder="URL" type="text">
                                        @if ($errors->has('video'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('video') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Описание</label>
                                    <div class="col-md-10">
                                        <textarea name="desc" class="form-control" placeholder="" rows="4">{{ old('desc') ?? $campaign->desc }}</textarea>
                                        @if ($errors->has('desc'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('desc') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Правила</label>
                                    <div class="col-md-10">
                                        <textarea class="form-control" placeholder="Необязательно" rows="4"></textarea>
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Социальная сеть</label>
                                    <div class="col-md-10">
                                        <select class="form-control" id="select-1">
                                            <option>Выберите соц. сеть</option>
                                            <option>VK</option>
                                            <option>YouTube</option>
                                            <option>Одноклассники</option>
                                        </select>
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Бюджет</label>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <span class="input-group-addon">Доступный бюджет :</span>
                                            <input value="{{ old('budget') ?? $campaign->budget }}" name="budget" class="form-control" type="text">
                                            <span class="input-group-addon">руб.</span>
                                        </div>
                                        @if ($errors->has('budget'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('budget') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Цена</label>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <span class="input-group-addon">Цена за один просмотр :</span>
                                            <input value="{{ old('price') ?? $campaign->price }}" name="price" class="form-control" type="text">
                                            <span class="input-group-addon">руб.</span>
                                        </div>
                                        @if ($errors->has('price'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('price') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>


                            </fieldset>

                            <fieldset>
                                <legend>Материалы</legend>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Постер</label>
                                    <div class="col-md-10">
                                        <input name="image" id="exampleInputFile1" type="file" class="btn btn-primary">
                                        @if ($errors->has('image'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('image') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                            </fieldset>

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12">
                                        <button class="btn btn-primary" type="submit">
                                            <i class="fa fa-save"></i>
                                            Сохранить
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div>
                    <!-- end widget content -->

                </div>


            </div>

        </div>

        <!-- end row -->

        <!-- END #MAIN CONTENT -->

    </div>
@stop