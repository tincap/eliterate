@extends('admin::layouts.master')

@section('title', 'ticket chat')

@section('content')
    <div id="main" role="main">

        <!-- RIBBON -->
        <div id="ribbon">

            <!-- breadcrumb -->
            <ol class="breadcrumb">
                <li>Главная</li>
                <li>Просмотр новости</li>
            </ol>

        </div>
        <!-- END RIBBON -->

        <!-- #MAIN CONTENT -->

        <!-- col -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark">

                <!-- PAGE HEADER -->
                <i class="fa-fw fa fa-ticket"></i>
                Чат тикета
            </h1>
        </div>
        <!-- end col -->

        <div class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

            <div class="jarviswidget well" >


                <div role="content">


                    <!-- widget content -->
                    <div class="widget-body">

                        <legend>{{ $ticket->title }}</legend>

                        <div class="row">
                            <div class="col-sm-1">

                                <div class="container-fluid">
                                    <img src="{{ asset('img/demo/e-comm/1.png') }}" width="50" height="50">
                                </div>

                            </div>

                            <div class="col-sm-11">

                                <div class="container-fluid">

                                    <div class="row">
                                        <div class="container-fluid">
                                            <p class="pull-left text-muted">
                                                {{ $ticket->user->name }}
                                            </p>
                                            <p class="pull-right text-muted">
                                                {{ $ticket->created_at->format('d.m.Y h:i') }}
                                            </p>
                                        </div>
                                    </div>

                                    <p>{{ $ticket->message }}</p>
                                    <p>&nbsp;</p>

                                </div>

                            </div>
                        </div>

                        <hr>

                        @foreach($ticket->messages as $message)

                            <div class="row">
                                <div class="col-sm-1">

                                    <div class="container-fluid">
                                        <img src="{{ asset('img/demo/e-comm/1.png') }}" width="50" height="50">
                                    </div>

                                </div>

                                <div class="col-sm-11">

                                    <div class="container-fluid">

                                        <div class="row">
                                            <div class="container-fluid">
                                                <p class="pull-left text-muted">
                                                    {{ $message->user->name }}
                                                </p>
                                                <p class="pull-right text-muted">
                                                    {{ $message->created_at->format('d.m.Y h:i') }}
                                                </p>
                                            </div>
                                        </div>

                                        <p>{{ $message->message }}</p>
                                        <p>&nbsp;</p>

                                    </div>

                                </div>
                            </div>

                            <legend></legend>

                        @endforeach

                        <div class="row">
                            <div class="col-sm-1">

                            </div>
                            <div class="col-sm-11">
                                <div class="container-fluid">
                                    <form method="POST">
                                        {{ csrf_field() }}
                                        <fieldset>
                                            <div class="form-group">
                                                <label class="control-label">Сообщение</label>
                                                <textarea name="message" class="form-control" placeholder="" rows="4"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <button class="btn btn-primary" type="submit">
                                                    Добавить комментарий
                                                </button>
                                            </div>
                                        </fieldset>
                                        <input name="user_id" type="hidden" value="{{ \Auth::id() }}">
                                        <input name="ticket_id" type="hidden" value="{{ $ticket->id }}">
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- end widget content -->

                </div>


            </div>

        </div>

        <!-- end row -->

        <!-- END #MAIN CONTENT -->

    </div>
@stop