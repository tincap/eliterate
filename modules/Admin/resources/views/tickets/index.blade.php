@extends('admin::layouts.master')

@section('title', 'ticket list')

@section('content')
    <div id="main" role="main">

        <!-- RIBBON -->
        <div id="ribbon">

            <!-- breadcrumb -->
            <ol class="breadcrumb">
                <li>Главная</li>
                <li>Список тикетов</li>
            </ol>

        </div>
        <!-- END RIBBON -->

        <!-- #MAIN CONTENT -->

        <!-- col -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark">

                <!-- PAGE HEADER -->
                <i class="fa-fw fa fa-ticket"></i>
                Список тикетов
            </h1>
        </div>
        <!-- end col -->

        <div class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

            <div class="jarviswidget well">

                <div role="content">

                    <!-- widget content -->
                    <div class="widget-body">

                        <div class="radio">
                            @foreach($statuses as $status => $name)
                                <label>
                                    @if(request('status') === $status)
                                        <input type="radio" name="status" value="{{ $status }}" checked>
                                    @else
                                        <input type="radio" name="status" value="{{ $status }}">
                                    @endif
                                    {{ $name }}
                                </label> &nbsp;
                            @endforeach
                        </div>

                        <legend></legend>

                        <table id="myTable">
                            <thead>
                            <tr>
                                <th>№</th>
                                <th>Создан</th>
                                <th>Тема</th>
                                <th>Ответов</th>
                                <th>Действия</th>
                            </tr>
                            </thead>
                            <tbody>
                            @include('admin::tickets._list')
                            </tbody>
                        </table>

                    </div>
                    <!-- end widget content -->

                </div>


            </div>

        </div>

        <!-- end row -->

        <!-- END #MAIN CONTENT -->

    </div>

    @push('js')
        <script type="text/javascript">
            $(function () {
                $('#myTable').DataTable({
                    "processing": true,
                    "responsive": true,
                    "language": {
                        "lengthMenu": "Показывать _MENU_ записей на странице",
                        "zeroRecords": "Записей не найдено",
                        "loadingRecords": "Загрузка... может занять несколько секунд...",
                        "info": "Страница _PAGE_ из _PAGES_",
                        "infoEmpty": "Показано с 0 по 0 из 0 записей",
                        "search": "Поиск",
                        "infoFiltered": "(Найдено записей: _TOTAL_)",
                        "sInfo": "Показано с _START_ по _END_ из _TOTAL_ записей",
                        "paginate":
                            {
                                "first": "Первая",
                                "last": "Последняя",
                                "next": "Следующая",
                                "previous": "Предыдущая"
                            }
                    },
                    "aaSorting": [[0, "desc"]],
                    "iDisplayLength": 10
                });

                $('.widget-body .radio input').on('click', function () {
                    var url = window.location.origin + window.location.pathname

                    url += '?' + $(this).attr('name') + '=' + $(this).attr('value')

                    window.location = url
                })
            });
        </script>
    @endpush
@stop