<?php

Route::group([
    'middleware' => ['web', 'admin'],
    'namespace'  => 'Modules\Admin\Http\Controllers',
    'prefix'     => 'admin',
    'as'         => 'admin.'
], function () {
    Route::get('/', 'AdminController@index')->name('admin');

    Route::resource('news', 'NewsController')->parameters([
        'news' => 'article'
    ]);

    Route::group(['prefix' => '/offers', 'as' => 'offers.'], function () {
        Route::get('/list', 'OfferController@index')->name('list');

        Route::delete('/delete/{id}', 'OfferController@destroy')->name('destroy');

        Route::get('/edit/{id}', 'OfferController@edit')->name('edit');
        Route::post('/edit/{id}', 'OfferController@update');

        Route::post('/landing/delete', 'OfferController@destroyLanding')->name('destroy.landing');
        Route::post('/rate/delete', 'OfferController@destroyRate')->name('destroy.rate');

        Route::get('/create', 'OfferController@create')->name('create');
        Route::post('/create', 'OfferController@store');

        Route::group(['prefix' => '/categories', 'as' => 'categories.'], function () {
            Route::get('/', 'CategoryController@index')->name('index');

            Route::get('/{category}/edit', 'CategoryController@edit')->name('edit');
            Route::post('/{category}/edit', 'CategoryController@update');

            Route::get('/create', 'CategoryController@create')->name('create');
            Route::post('/create', 'CategoryController@store');

            Route::delete('/{category}', 'CategoryController@destroy')->name('destroy');
        });
    });

    Route::group(['prefix' => 'users', 'as' => 'users.'], function () {
        Route::get('/', 'UserController@index')->name('index');
        Route::get('/{user}/edit', 'UserController@edit')->name('edit');
        Route::post('/{user}/edit', 'UserController@update');
        Route::delete('/{user}', 'UserController@destroy')->name('destroy');

        Route::get('/platform/confirm/{platform}', 'UserController@confirmPlatform')->name('platform.confirm');
        Route::get('/platform/delete/{platform}', 'UserController@destroyPlatform')->name('platform.destroy');

        Route::get('/platforms', 'UserController@getPlatforms')->name('platforms');
        Route::post('/platforms', 'UserController@storePlatform');

        Route::get('/statistics', 'UserController@getStatistics')->name('statistics');
    });

    Route::group(['prefix' => 'tickets', 'as' => 'tickets.'], function () {
        Route::get('/', 'TicketController@index')->name('index');
        Route::get('/create', 'TicketController@create')->name('create');
        Route::post('/create', 'TicketController@store')->name('store');

        Route::get('/chat', 'TicketController@chat')->name('chat');
        Route::post('/chat', 'TicketController@storeChat');
    });

    Route::group(['prefix' => 'creatives', 'as' => 'creatives.'], function () {
        Route::get('/{creative}/edit', 'CreativeController@edit')->name('edit');
        Route::post('/{creative}/edit', 'CreativeController@update');

        Route::get('/create/{offer}', 'CreativeController@create')->name('create');
        Route::post('/create/{offer}', 'CreativeController@store');

        Route::get('/{creative}/delete', 'CreativeController@destroy')->name('destroy');

        Route::post('/remove-upload', 'CreativeController@removeUpload')->name('removeUpload');
    });

    Route::group(['prefix' => 'countries', 'as' => 'countries.'], function () {
        Route::get('/', 'CountryController@index')->name('index');

        Route::get('/create', 'CountryController@create')->name('create');
        Route::post('/create', 'CountryController@store');

        Route::get('/{country}/edit', 'CountryController@edit')->name('edit');
        Route::post('/{country}/edit', 'CountryController@update');

        Route::get('/{country}/delete', 'CountryController@destroy')->name('destroy');
    });

    Route::group(['prefix' => 'campaigns', 'as' => 'campaigns.'], function () {
        Route::get('/', 'CampaignController@index')->name('index');

        Route::get('/{campaign}/edit', 'CampaignController@edit')->name('edit');
        Route::post('/{campaign}/edit', 'CampaignController@update');

        Route::get('/create', 'CampaignController@create')->name('create');
        Route::post('/create', 'CampaignController@store');

        Route::get('/{campaign}/delete', 'CampaignController@destroy')->name('destroy');
    });

    Route::group(['prefix' => '/wallets', 'as' => 'wallets.'], function () {
        Route::get('/', 'WalletController@index')->name('index');
    });
});
