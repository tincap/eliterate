<?php

namespace Modules\Admin\Http\Requests\Creatives;

use Illuminate\Foundation\Http\FormRequest;

class StoreCreativesRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'   => 'required|string|max:255',
            'text'   => 'required|string|max:2000',
            'images' => 'required|json|max:2000'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
