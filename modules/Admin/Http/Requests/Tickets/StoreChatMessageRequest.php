<?php

namespace Modules\Admin\Http\Requests\Tickets;

use Illuminate\Foundation\Http\FormRequest;

class StoreChatMessageRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'message'   => 'required|max:2000|string',
            'ticket_id' => 'required|integer|exists:tickets,id',
            'user_id'   => 'required|integer|exists:users,id'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
