<?php

namespace Modules\Admin\Http\Requests\Campaigns;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCampaignRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'   => 'required|string|max:255',
            'video'  => 'required|url|max:255',
            'image'  => 'sometimes|image',
            'desc'   => 'required|string|max:2000',
            'price'  => 'required|numeric',
            'views'  => 'required|integer',
            'budget' => 'required|numeric'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
