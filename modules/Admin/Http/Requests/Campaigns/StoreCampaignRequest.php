<?php

namespace Modules\Admin\Http\Requests\Campaigns;

use Illuminate\Foundation\Http\FormRequest;

class StoreCampaignRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'   => 'required|string|max:255',
            'slug'   => 'required|string|max:255|unique:campaigns',
            'video'  => 'required|url|max:255',
            'image'  => 'required|image',
            'desc'   => 'required|string|max:2000',
            'price'  => 'required|numeric',
            'views'  => 'required|integer',
            'budget' => 'required|numeric'
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge(['slug' => str_slug($this->name)]);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
