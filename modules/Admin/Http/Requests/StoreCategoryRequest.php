<?php

namespace Modules\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCategoryRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('category') ? $this->route('category')->id : null;

        return [
            'name'  => 'required|string|max:255',
            'slug'  => 'required|string|max:255|unique:categories,slug'.($id ? ",$id" : ''),
            'order' => 'nullable|integer'
        ];
    }

    protected function prepareForValidation()
    {
        if (! $this->slug) {
            $this->merge(['slug' => str_slug($this->name)]);
        }
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
