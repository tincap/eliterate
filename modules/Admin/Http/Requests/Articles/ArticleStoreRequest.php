<?php

namespace Modules\Admin\Http\Requests\Articles;

use Illuminate\Foundation\Http\FormRequest;

class ArticleStoreRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required|between:5,255',
            'text'  => 'required',
            'image' => 'required|image'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
