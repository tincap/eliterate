<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Str;
use Modules\Platforms\Entities\Category;
use Modules\Platforms\Entities\Landing;
use Modules\Platforms\Entities\Offer;
use Modules\Platforms\Entities\Rate;
use Modules\Platforms\Entities\Traffic;
use Modules\Platforms\Partners\PartnerPrograms;
use Modules\Users\Entities\Country;

class OfferController extends Controller
{

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $offers = Offer::orderBy('name')->get();

        return view('admin::offers.index', compact('offers'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $countries  = Country::all();
        $traffics   = Traffic::all();
        $categories = Category::prepared()->get();
        $partners   = \Modules\Platforms\Partners\PartnerPrograms::getPrograms();

        return view('admin::offers.create', compact('countries', 'traffics', 'categories', 'partners'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name'        => 'required|max:64',
            'link'        => 'required',
            'description' => 'required',
            'image'       => 'required',
            'category_id' => 'required|exists:categories,id',
        ], [
            'required'    => 'Поле :attribute обязательно',
            'category_id' => 'Поле :attribute некоректно',
        ], [
            'name'        => 'Заголовок',
            'description' => 'Описание',
            'link'        => 'Ссылка',
            'image'       => 'Постер',
            'category_id' => 'Категория',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $img = $request->file('image');

        $offer = new Offer($request->only([
            'name',
            'target',
            'link',
            'description',
            'category_id',
            'rules',
        ]));

        $offer->image       = $img->storeAs('public/offers', uniqid().'.'.$img->getClientOriginalExtension());
        $offer->slug        = Str::slug($request->get('name'));
        $offer->has_landing = (bool)$request->has('has_landing');
        $offer->save();

        if ($request->get('rates')) {
            foreach (json_decode($request->get('rates')) as $i) {
                $offer->rates()->create((array)$i);
            }
        }

        if ($request->get('landings')) {
            foreach (json_decode($request->get('landings')) as $i) {
                $offer->landings()->create((array)$i);
            }
        }

        if ($request->get('partner_type', false)) {
            $program = PartnerPrograms::getProgram($request->get('partner_type'));

            if ($program) {
                $offer->partner()->create([
                    'partner_id'   => $program::getOfferIdByUrl($request->get('partner_link')),
                    'partner_type' => $request->get('partner_type'),
                ]);
            }
        }

        $offer->traffics()->attach(array_map('intval', $request->get('traffic', [])));

        return redirect(route('admin.offers.edit', ['id' => $offer->id]));
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $offer      = Offer::find($id);
        $countries  = Country::all();
        $traffics   = Traffic::all();
        $categories = Category::prepared()->get();
        $partners   = \Modules\Platforms\Partners\PartnerPrograms::getPrograms();

        return view('admin::offers.update', compact('partners', 'offer', 'countries', 'traffics', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name'        => 'required|max:64',
            'link'        => 'required',
            'description' => 'required',
            'category_id' => 'required|exists:categories,id',
        ], [
            'required'    => 'Поле :attribute обязательно',
            'category_id' => 'Поле :attribute некоректно',
        ], [
            'name'        => 'Заголовок',
            'description' => 'Описание',
            'link'        => 'Ссылка',
            'category_id' => 'Категория',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        Offer::where('id', $id)->update($request->only([
            'name',
            'target',
            'link',
            'description',
            'category_id',
            'rules',
        ]));

        $offer = Offer::find($id);

        if ($request->hasFile('image')) {
            $img          = $request->file('image');
            $offer->image = $img->storeAs('public/offers', uniqid().'.'.$img->getClientOriginalExtension());
        }

        $offer->slug        = Str::slug($request->get('name'));
        $offer->has_landing = (bool)$request->has('has_landing');
        $offer->save();

        if ($request->get('rates')) {
            foreach (json_decode($request->get('rates')) as $i) {
                if (isset($i->id) && $i->id) {
                    $offer->rates()->where('id', $i->id)->update((array)$i);
                } else {
                    $offer->rates()->create((array)$i);
                }
            }
        }

        if ($request->get('landings')) {
            foreach (json_decode($request->get('landings')) as $i) {
                if (isset($i->id) && $i->id) {
                    $offer->landings()->where('id', $i->id)->update((array)$i);
                } else {
                    $offer->landings()->create((array)$i);
                }
            }
        }

        $offer->traffics()->sync(array_map('intval', $request->get('traffic', [])));

        if ($request->get('partner_type', false)) {
            $program = PartnerPrograms::getProgram($request->get('partner_type'));

            if ($program) {
                if ($offer->partner) {
                    $offer->partner()->update([
                        'partner_id'   => $program::getOfferIdByUrl($request->get('partner_link')),
                        'partner_type' => $request->get('partner_type'),
                    ]);
                } else {
                    $offer->partner()->create([
                        'partner_id'   => $program::getOfferIdByUrl($request->get('partner_link')),
                        'partner_type' => $request->get('partner_type'),
                    ]);
                }
            }
        }

        return redirect()->back();
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $offer = Offer::find($id);
        $offer->delete();

        return \response('ok');
    }

    public function getCategories()
    {
        return view('admin::offers.categories');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function destroyLanding(Request $request)
    {
        $landing = Landing::find($request->get('landing_id'));
        $landing->delete();

        return \response('ok');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function destroyRate(Request $request)
    {
        $rate = Rate::find($request->get('rate_id'));
        $rate->delete();

        return \response('ok');
    }
}
