<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Admin\Http\Requests\Creatives\StoreCreativesRequest;
use Modules\Platforms\Entities\Creative;
use Modules\Platforms\Entities\Offer;

class CreativeController extends Controller
{

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('admin::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Creative $creative)
    {
        return view('admin::creatives.edit', compact('creative'));
    }

    /**
     * Store a newly created resource in storage.
     * @param StoreCreativesRequest $request
     * @return Response
     * @internal param $
     */
    public function store(Offer $offer, StoreCreativesRequest $request)
    {
        $creative = $offer->creatives()->make($request->except('images'));

        $images = collect(json_decode($request->get('images', [])));

        $creative->images = $images->map(function ($item) {
            return "public/upload/images/$item";
        });

        $creative->save();

        return redirect(route('admin.offers.edit', ['id' => $offer->id]));
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Creative $creative)
    {
        return view('admin::creatives.edit', compact('creative'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, Creative $creative)
    {
        $images = collect(json_decode($request->get('images', [])));

        $images = $images->map(function ($item) {
            return "public/upload/images/$item";
        });

        $creative->images = $images->concat($creative->images)->unique();

        $creative->save();

        return back();

    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Creative $creative)
    {
        foreach ($creative->images as $image) {
            \Storage::delete($image);
        }

        $status = $creative->delete();

        return redirect()->back()->with('success', $status);
    }

    public function removeUpload(Request $request)
    {
        $search = "public/upload/images/{$request->get('file')}";

        if ($request->get('id', false)) {
            $creative = Creative::find($request->get('id'));

            $images = collect($creative->images);

            $creative->images = $images->each(function ($item, $key) use ($search, $images) {
                if ($item === $search) {
                    $images->forget($key);
                };
            });

            $creative->save();
        }

        $status = \Storage::delete($search);

        return response()->json(['success', $status], 200);
    }
}
