<?php

namespace Modules\Platforms\Entities;

use Illuminate\Database\Eloquent\Model;
use Storage;

/**
 * Class Article
 * @package App
 */
class Article extends Model
{

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'text',
        'image'
    ];

    /**
     * @var string
     */
    protected $table = 'news';

    /**
     * @return string
     */
    public function getImageUrlAttribute()
    {
        return Storage::url($this->image);
    }
}
