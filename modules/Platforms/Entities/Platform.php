<?php

namespace Modules\Platforms\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Users\Entities\User;

class Platform extends Model
{

    const TYPE_VK = 'vk';

    protected $fillable = ['user_id', 'name', 'profile', 'url', 'type', 'price'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
