<?php

namespace Modules\Platforms\Entities;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{

    const TARGET_CLICK = 'click';
    const TARGET_POSTBACK = 'postback';

    const COUNTRY_UNDEFINED = 'undefined';

    const STATUS_SUCCESS = 'success';
    const STATUS_WAITING = 'waiting';
    const STATUS_FAILED  = 'failed';

    const STATUS_UNDEFINED = 'undefined';

    protected $fillable = [
        'offer_id',
        'stream_id',
        'subid_id',
        'target',
        'ip',
        'partner',
        'order_id',
        'country',
        'from_url',
        'log',
        'status'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function stream()
    {
        return $this->belongsTo(Stream::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subid()
    {
        return $this->belongsTo(Subid::class);
    }
}
