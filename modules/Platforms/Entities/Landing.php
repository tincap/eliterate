<?php

namespace Modules\Platforms\Entities;

use Illuminate\Database\Eloquent\Model;

class Landing extends Model
{

    protected $fillable = ['offer_id', 'name', 'url'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function offer()
    {
        return $this->belongsTo(Offer::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function streams()
    {
        return $this->hasMany(Stream::class);
    }
}
