<?php

namespace Modules\Platforms\Entities;

use Illuminate\Database\Eloquent\Model;

class Traffic extends Model
{

    protected $fillable = ['name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function offers()
    {
        return $this->belongsToMany(Offer::class);
    }
}
