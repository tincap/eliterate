<?php

namespace Modules\Platforms\Entities;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name', 'slug', 'order'];

    /**
     * @param Builder $builder
     * @return Builder $this
     */
    public function scopePrepared($builder)
    {
        return $builder->where('isActive', true)->orderBy('order');
    }

    public function offers()
    {
        return $this->hasMany(Offer::class);
    }
}
