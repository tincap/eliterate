<?php

namespace Modules\Platforms\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Platforms\Entities\Article;
use Faker\Generator as Faker;

/**
 * Class NewsTableSeeder
 */
class NewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        foreach (range(1, 50) as $i) {
            Article::create([
                'name' => $faker->text(15),
                'text' => $faker->text(),
                'image' => 'public/img/human-1.png'
            ]);
        }
    }
}
