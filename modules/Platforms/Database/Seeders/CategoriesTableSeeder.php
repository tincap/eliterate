<?php

namespace Modules\Platforms\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Platforms\Entities\Category;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Category::truncate();

        $categories = ['ВИДЕПОСЕВ', 'ТОВАРЫ ПОЧТОЙ', 'АДАЛТ', 'ГЕМБЛИНГ', 'ПРИЛОЖЕНИЯ', 'ДЕЙТИНГ'];

        foreach ($categories as $category) {
            Category::create([
                'name' => $category,
                'slug' => str_slug($category)
            ]);
        }
    }
}
