<?php

namespace Modules\Platforms\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Users\Database\Seeders\CountryTableSeeder;

/**
 * Class PlatformsDatabaseSeeder
 * @package Modules\Platforms\Database\Seeders
 */
class PlatformsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(CategoriesTableSeeder::class);
        $this->call(PlatformTableSeeder::class);
        $this->call(TrafficTableSeeder::class);
        $this->call(CountryTableSeeder::class);
        $this->call(NewsTableSeeder::class);
        $this->call(OffersTableSeeder::class);
        $this->call(StreamTableSeeder::class);
        $this->call(CreativesTableSeeder::class);
    }
}
