<?php

namespace Modules\Platforms\Database\Seeders;

use Faker\Generator;
use Illuminate\Database\Seeder;
use Modules\Platforms\Entities\Category;
use Modules\Platforms\Entities\Offer;
use Modules\Platforms\Entities\Rate;
use Modules\Users\Entities\Country;

class OffersTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Generator $faker)
    {
        Offer::truncate();
        Rate::truncate();

        foreach (range(1, 30) as $i) {
            $offer = \Modules\Platforms\Entities\Offer::create([
                'name'        => $faker->text(15),
                'slug'        => $faker->unique()->slug(),
                'image'       => 'img/human-1.png',
                'link'        => $faker->url,
                'target'      => $faker->word,
                'description' => $faker->realText(),
                'category_id' => Category::all()->random()->id
            ]);

            foreach (range(1, rand(1, 3)) as $k) {
                Rate::create([
                    'offer_id'   => $offer->id,
                    'country_id' => Country::find($k)->id,
                    'rate'       => $faker->randomFloat(2, 0, 5),
                    'price'      => $faker->randomFloat(2, 0, 5)
                ]);
            }
        }
    }
}
