<?php

namespace Modules\Platforms\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Platforms\Entities\Event;
use Modules\Platforms\Entities\Landing;
use Modules\Platforms\Entities\Stream;
use Modules\Platforms\Partners\Partner;
use Modules\Platforms\Partners\PartnerPrograms;

class TrafficController extends Controller
{

    /**
     * @param string $hash
     * @return Response
     */
    public function redirectUser($hash, $subid = null, Request $request)
    {
        /** @var Stream $stream */
        $stream = Stream::where('hash', $hash)->firstOrFail();
        $offer  = $stream->offer;

        if ($subid) {
            $subid = $stream->subids()->where('name', $subid)->first();
        }

        if ($partner = $offer->partner) {
            /** @var Partner $handler */
            if ($handler = $partner->getPartner()) {
                try {
                    $link = array_random($handler->getLinksFromOffer($partner->partner_id));
                    Event::create([
                        'order_id'  => uniqid(),
                        'partner'   => PartnerPrograms::PARTNER_NONE,
                        'stream_id' => $stream->id,
                        'offer_id'  => $offer->id,
                        'subid_id'  => $subid ? $subid->id : null,
                        'status'    => Event::STATUS_WAITING,
                        'target'    => Event::TARGET_POSTBACK,
                        'from_url'  => $request->header('HTTP_REFERER', null),
                        'to_url'    => $link,
                        'country'   => isset($GEOIP_code) ? $GEOIP_code : Event::COUNTRY_UNDEFINED,
                        'ip'        => $request->getClientIp(),
                    ]);

                    return redirect($link);
                } catch (\Exception $e) {
                    \Log::warning($e->getMessage());
                }
            }
        }

        return $this->redirectToLanding($stream, $request);
    }

    protected function redirectToLanding(Stream $stream, Request $request)
    {
        if ($stream->landing) {
            $url = $stream->landing->url;
        } else {
            $url = $stream->offer->landings->random()->url;
        }

        Event::create([
            'offer_id'  => $stream->offer_id,
            'stream_id' => $stream->id,
            'status'    => Event::STATUS_SUCCESS,
            'target'    => Event::TARGET_CLICK,
            'from_url'  => $request->header('HTTP_REFERER', null),
            'to_url'    => $url,
            'country'   => isset($GEOIP_code) ? $GEOIP_code : Event::COUNTRY_UNDEFINED,
            'ip'        => $request->getClientIp(),
        ]);

        return redirect($url);
    }
}
