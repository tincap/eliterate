<?php

namespace Modules\Platforms\Http\Controllers;

use Illuminate\Routing\Controller;

class StatisticsController extends Controller
{
    public function index()
    {
        return view('platforms::statistics.index');
    }

    public function getVideoStatistics()
    {
        return view('platforms::statistics.video');
    }
}
