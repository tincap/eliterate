<?php

namespace Modules\Platforms\Http\Controllers;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Modules\Platforms\Entities\Article;
use Modules\Platforms\Entities\Category;
use Modules\Platforms\Entities\Offer;
use Modules\Platforms\Entities\Traffic;
use Modules\Users\Entities\Country;

/**
 * Class OfferController
 * @package Modules\Platforms\Http\Controllers
 */
class OfferController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $page = $request->ajax() ? 'list' : 'index';

        if ($request->get('category')) {
            $offers = Category::where('slug', $request->get('category'))
                ->firstOrFail()
                ->offers();

            $activeCategory = $request->get('category');
        } else {
            $offers = Offer::query();

            $activeCategory = 'all';
        }

        if ($request->has('search')) {
            $search = "%{$request->search}%";

            $offers = $offers->where(function (Builder $query) use ($search) {
                return $query->where('name', 'like', $search)
                    ->orWhere('target', 'like', $search)
                    ->orWhereHas('rates', function (Builder $q) use ($search) {
                        $q->where('rate', 'like', $search)
                            ->orWhere('price', 'like', $search)
                            ->orWhereHas('country', function (Builder $q) use ($search) {
                                $q->where('name', 'like', $search);
                            });
                    });
            });
        }

        $offers = $offers->with(['rates.country'])->paginate(10);

        $categories = $page === 'list' ? null : Category::prepared()->get();
        $countries  = Country::all();

        return view("platforms::offers.$page", compact('offers', 'categories', 'activeCategory', 'countries'));
    }

    /**
     * @param Offer $offer
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Offer $offer)
    {
        $offer->load(['traffics', 'rates.country']);

        $news = Article::latest()->limit(2)->get();

        return view('platforms::offers.show', [
            'offer'    => $offer,
            'traffics' => Traffic::all(),
            'news'     => $news
        ]);
    }
}
