<?php

namespace Modules\Platforms\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Platforms\Entities\Platform;
use Modules\Platforms\Socials\SuperVK;
use Modules\Users\Entities\User;

class PlatformsController extends Controller
{

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $user      = Auth::user();
        $platforms = $user->platforms()->orderByDesc('created_at')->get();

        return view('platforms::index', compact('platforms'));
    }

    /**
     * List of search platforms.
     *
     * @return Response
     */
    public function show(Request $request)
    {
        $user = Auth::user();
        if ($request->get('search', false)) {
            $search    = $request->get('search');
            $platforms = $user->platforms()->where(function ($q) use ($search) {
                return $q->where('name', 'like', '%'.$search.'%')
                    ->orWhere('profile', 'like', '%'.$search.'%')
                    ->orWhere('url', 'like', '%'.$search.'%');
            })->get();
        } else {
            $platforms = $user->platforms;
        }

        return view('platforms::platform_list', compact('platforms'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $user     = Auth::user();
        $platform = new Platform();

        $vk = new SuperVK($user);

        $vk_link = $vk->getLoginUrl(route('user.settings'));

        if ($vk->isAuth()) {
            $query = 'return {"returned": [API.users.get(),API.friends.get(),
        API.groups.get({"filter": "moder,editor,admin","extended": 1,"version": "5.19"})]};';

            $response = $vk->api('execute', ['code' => $query, 'version' => '5.74']);

            $groups = $response['response']['returned'][2];
            $groups = array_filter($groups, 'is_array');

            $groups = array_splice($groups, 0, 24);
            $query  = 'return {"returned": [';

            $groups = array_map(function ($group) use (&$query) {
                $query .= sprintf('API.groups.getMembers({"group_id": "%s"}),', $group['gid']);

                return $group;
            }, $groups);

            $query .= ']};';

            $groups_count = $vk->api('execute', ['code' => $query, 'version' => '5.74'])['response']['returned'];


            $groups = array_map(function ($group, $index) use ($groups_count) {
                $group['count'] = isset($groups_count[$index]) ? $groups_count[$index]['count'] : 0;

                return $group;
            }, $groups, array_keys($groups));


            $account = $response['response']['returned'][0][0];

            $groups[] = [
                'gid'   => null,
                'name'  => $account['last_name'].' '.$account['first_name'],
                'count' => count($response['response']['returned'][1])
            ];

            $used_groups = $user->platforms()->where('type', Platform::TYPE_VK)
                ->get()->pluck('profile')->toArray();

            $available_groups = array_filter($groups, function ($g) use ($used_groups) {
                return ! in_array($g['gid'], $used_groups);
            });
        } else {
            $platform         = [];
            $available_groups = [];
        }

        return view('platforms::create', compact('platform', 'available_groups', 'vk_link'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        if (! $request->has('profile')) {
            return redirect()->back();
        }
        /** @var User $user */
        $user = Auth::user();
        $vk   = new SuperVK($user);

        $non_unqiue_platforms = $user->platforms()->where('type', Platform::TYPE_VK)
            ->where('profile', $request->get('profile'))->get();

        if ($non_unqiue_platforms->isNotEmpty()) {
            return redirect()->back();
        }

        if ($request->get('profile')) {
            try {
                $response = $vk->api('groups.getById', [
                    'group_ids' => $request->get('profile'),
                    'version'   => 5.74
                ])['response'][0];

                $user->platforms()->create([
                    'name'    => $response['name'],
                    'profile' => $response['gid'],
                    'url'     => 'http://vk.com/'.$response['screen_name'],
                    'type'    => Platform::TYPE_VK,
                ]);

            } catch (\Exception $e) {
                return redirect()->back();
            }
        } else {
            try {
                $response = $vk->api('users.get', [
                    'version' => 5.74
                ])['response'][0];

                $user->platforms()->create([
                    'name' => $response['last_name'].' '.$response['first_name'],
                    'url'  => 'http://vk.com/id'.$response['uid'],
                    'type' => Platform::TYPE_VK,
                ]);

            } catch (\Exception $e) {
                return redirect()->back();
            }
        }

        return redirect(route('user.platforms'));
    }


    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('platforms::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
