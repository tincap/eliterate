<?php

namespace Modules\Platforms\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Users\Rules\UserBalance;

class StorePaymentRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'balance'   => ['required', 'integer', new UserBalance()],
            'wallet_id' => 'required|integer|exists:wallets,id'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
