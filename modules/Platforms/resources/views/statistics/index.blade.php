@extends('layouts.master')

@section('content')
    <section class="page-content statistics page-responsive">
        <div class="container">
            <div class="page-head">
                <div class="page-head__name">
                    <h1>Статистика</h1>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/">Главная</a></li>
                            <li class="breadcrumb-item active" aria-current="page">
                                <a href="{{ route('statistics') }}">Статистика</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <ul class="tabs">
                <li><a class="tabs-active">офферы</a></li>
                <li><a href="{{ route('statistics.video') }}">видео-кампании</a></li>
            </ul>

            <div class="form-row statistics-select">
                <select class="form-control">
                    <option selected="">28.03.2018-04.04.218</option>
                    <option>123</option>
                    <option>123</option>
                    <option>123</option>
                    <option>123</option>
                </select>
                <select class="form-control">
                    <option selected="">Площадки</option>
                    <option>Площадки1</option>
                    <option>Площадки2</option>
                    <option>Площадки3</option>
                    <option>Площадки4</option>
                </select>
                <select class="form-control">
                    <option selected="">Площадки</option>
                    <option>Офферы1</option>
                    <option>Офферы2</option>
                    <option>Офферы3</option>
                    <option>Офферы4</option>
                </select>
                <select class="form-control">
                    <option selected="">Sub ID</option>
                    <option>ID1</option>
                    <option>ID2</option>
                    <option>ID3</option>
                    <option>ID4</option>
                </select>
                <select class="form-control">
                    <option selected="">Потоки</option>
                    <option>Потоки1</option>
                    <option>Потоки2</option>
                    <option>Потоки3</option>
                    <option>Потоки4</option>
                </select>
                <select class="form-control">
                    <option selected="">Страны</option>
                    <option>Страны1</option>
                    <option>Страны2</option>
                    <option>Страны3</option>
                    <option>Страны4</option>
                </select>
                <select class="form-control">
                    <option selected="">RUB</option>
                    <option>RUB1</option>
                    <option>RUB2</option>
                    <option>RUB3</option>
                    <option>RUB4</option>
                </select>
            </div>


            <div class="responsive-table statistics-table__big">
                <div class="tab-pane fade show active" id="tab-offers" role="tabpanel" aria-labelledby="offer">
                    {{--<div class="form-row statistics-select">--}}
                    {{--<select class="form-control">--}}
                    {{--<option selected>28.03.2018-04.04.218</option>--}}
                    {{--<option>123</option>--}}
                    {{--<option>123</option>--}}
                    {{--<option>123</option>--}}
                    {{--<option>123</option>--}}
                    {{--</select>--}}
                    {{--<select class="form-control">--}}
                    {{--<option selected>Площадки</option>--}}
                    {{--<option>Площадки1</option>--}}
                    {{--<option>Площадки2</option>--}}
                    {{--<option>Площадки3</option>--}}
                    {{--<option>Площадки4</option>--}}
                    {{--</select>--}}
                    {{--<select class="form-control">--}}
                    {{--<option selected>Площадки</option>--}}
                    {{--<option>Офферы1</option>--}}
                    {{--<option>Офферы2</option>--}}
                    {{--<option>Офферы3</option>--}}
                    {{--<option>Офферы4</option>--}}
                    {{--</select>--}}
                    {{--<select class="form-control">--}}
                    {{--<option selected>Sub ID</option>--}}
                    {{--<option>ID1</option>--}}
                    {{--<option>ID2</option>--}}
                    {{--<option>ID3</option>--}}
                    {{--<option>ID4</option>--}}
                    {{--</select>--}}
                    {{--<select class="form-control">--}}
                    {{--<option selected>Потоки</option>--}}
                    {{--<option>Потоки1</option>--}}
                    {{--<option>Потоки2</option>--}}
                    {{--<option>Потоки3</option>--}}
                    {{--<option>Потоки4</option>--}}
                    {{--</select>--}}
                    {{--<select class="form-control">--}}
                    {{--<option selected>Страны</option>--}}
                    {{--<option>Страны1</option>--}}
                    {{--<option>Страны2</option>--}}
                    {{--<option>Страны3</option>--}}
                    {{--<option>Страны4</option>--}}
                    {{--</select>--}}
                    {{--<select class="form-control">--}}
                    {{--<option selected>RUB</option>--}}
                    {{--<option>RUB1</option>--}}
                    {{--<option>RUB2</option>--}}
                    {{--<option>RUB3</option>--}}
                    {{--<option>RUB4</option>--}}
                    {{--</select>--}}
                    {{--</div>--}}
                    <div class="responsive-table statistics-table__big">
                        <div class="card">
                            <div class="card-top__head">
                                <div class="row">
                                    <div class="col-3"></div>
                                    <div class="col-2">
                                        <p>Тарифы</p>
                                    </div>
                                    <div class="col-1">
                                        <p>Коэффициенты</p>
                                    </div>
                                    <div class="col-2">
                                        <p>Конверсии</p>
                                    </div>
                                    <div class="col-4">
                                        <p>Финансы</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-3">
                                        <p>Программа</p>
                                    </div>
                                    <div class="col-2">
                                        <div class="row">
                                            <div class="col-6">
                                                <p>Клипы</p>
                                            </div>
                                            <div class="col-6">
                                                <p>Хиты</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-1">
                                        <div class="row">
                                            <div class="col-6">
                                                <p>EPC</p>
                                            </div>
                                            <div class="col-6">
                                                <p>CR</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-2 card-icons">
                                        <ul>
                                            <li><a data-toggle="tooltip" data-placement="top" title="bars" href="#"><i
                                                            class="fa fa-bars" aria-hidden="true"></i></a></li>
                                            <li><a data-toggle="tooltip" data-placement="top" title="clock" href="#"><i
                                                            class="fa fa-clock-o" aria-hidden="true"></i></a></li>
                                            <li><a data-toggle="tooltip" data-placement="top" title="close" href="#"><i
                                                            class="fa fa-times" aria-hidden="true"></i></a></li>
                                            <li><a data-toggle="tooltip" data-placement="top" title="trash" href="#"><i
                                                            class="fa fa-trash-o" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="col-4">
                                        <ul>
                                            <li>Всего</li>
                                            <li>Принято</li>
                                            <li>Ожидает</li>
                                            <li>Отклонено</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-3">
                                        <p>Комплекс снижения веса</p>
                                    </div>
                                    <div class="col-2">
                                        <div class="row">
                                            <div class="col-6">
                                                <p>64</p>
                                            </div>
                                            <div class="col-6">
                                                <p>68</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-1">
                                        <div class="row">
                                            <div class="col-6">
                                                <p>0</p>
                                            </div>
                                            <div class="col-6">
                                                <p>0</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <ul>
                                            <li>2</li>
                                            <li>0</li>
                                            <li>0</li>
                                            <li>1</li>
                                        </ul>
                                    </div>
                                    <div class="col-4">
                                        <ul>
                                            <li>1365.0</li>
                                            <li>0.00</li>
                                            <li>0.00</li>
                                            <li>1365.0</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-3">
                                        <p>Комплекс снижения веса</p>
                                    </div>
                                    <div class="col-2">
                                        <div class="row">
                                            <div class="col-6">
                                                <p>64</p>
                                            </div>
                                            <div class="col-6">
                                                <p>68</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-1">
                                        <div class="row">
                                            <div class="col-6">
                                                <p>0</p>
                                            </div>
                                            <div class="col-6">
                                                <p>0</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <ul>
                                            <li>2</li>
                                            <li>0</li>
                                            <li>0</li>
                                            <li>1</li>
                                        </ul>
                                    </div>
                                    <div class="col-4">
                                        <ul>
                                            <li>1365.0</li>
                                            <li>0.00</li>
                                            <li>0.00</li>
                                            <li>1365.0</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-3">
                                        <p>Комплекс снижения веса</p>
                                    </div>
                                    <div class="col-2">
                                        <div class="row">
                                            <div class="col-6">
                                                <p>64</p>
                                            </div>
                                            <div class="col-6">
                                                <p>68</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-1">
                                        <div class="row">
                                            <div class="col-6">
                                                <p>0</p>
                                            </div>
                                            <div class="col-6">
                                                <p>0</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <ul>
                                            <li>2</li>
                                            <li>0</li>
                                            <li>0</li>
                                            <li>1</li>
                                        </ul>
                                    </div>
                                    <div class="col-4">
                                        <ul>
                                            <li>1365.0</li>
                                            <li>0.00</li>
                                            <li>0.00</li>
                                            <li>1365.0</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-3">
                                        <p>Комплекс снижения веса</p>
                                    </div>
                                    <div class="col-2">
                                        <div class="row">
                                            <div class="col-6">
                                                <p>64</p>
                                            </div>
                                            <div class="col-6">
                                                <p>68</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-1">
                                        <div class="row">
                                            <div class="col-6">
                                                <p>0</p>
                                            </div>
                                            <div class="col-6">
                                                <p>0</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <ul>
                                            <li>2</li>
                                            <li>0</li>
                                            <li>0</li>
                                            <li>1</li>
                                        </ul>
                                    </div>
                                    <div class="col-4">
                                        <ul>
                                            <li>1365.0</li>
                                            <li>0.00</li>
                                            <li>0.00</li>
                                            <li>1365.0</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-3">
                                        <p>Комплекс снижения веса</p>
                                    </div>
                                    <div class="col-2">
                                        <div class="row">
                                            <div class="col-6">
                                                <p>64</p>
                                            </div>
                                            <div class="col-6">
                                                <p>68</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-1">
                                        <div class="row">
                                            <div class="col-6">
                                                <p>0</p>
                                            </div>
                                            <div class="col-6">
                                                <p>0</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <ul>
                                            <li>2</li>
                                            <li>0</li>
                                            <li>0</li>
                                            <li>1</li>
                                        </ul>
                                    </div>
                                    <div class="col-4">
                                        <ul>
                                            <li>1365.0</li>
                                            <li>0.00</li>
                                            <li>0.00</li>
                                            <li>1365.0</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--<div class="tab-pane fade" id="tab-video" role="tabpanel" aria-labelledby="video">--}}
                {{--<div class="form-row statistics-select">--}}
                {{--<select class="form-control">--}}
                {{--<option selected>28.03.2018-04.04.218</option>--}}
                {{--<option>123</option>--}}
                {{--<option>123</option>--}}
                {{--<option>123</option>--}}
                {{--<option>123</option>--}}
                {{--</select>--}}
                {{--<select class="form-control">--}}
                {{--<option selected>Площадки</option>--}}
                {{--<option>Площадки1</option>--}}
                {{--<option>Площадки2</option>--}}
                {{--<option>Площадки3</option>--}}
                {{--<option>Площадки4</option>--}}
                {{--</select>--}}
                {{--</div>--}}
                {{--<div class="responsive-table statistics-table">--}}
                {{--<div class="card">--}}
                {{--<div class="card-header">--}}
                {{--<div class="row">--}}
                {{--<div class="col-4">--}}
                {{--<p>Площадка</p>--}}
                {{--</div>--}}
                {{--<div class="col-2">--}}
                {{--<p>Заработок</p>--}}
                {{--</div>--}}
                {{--<div class="col-2">--}}
                {{--<p>Просмотры</p>--}}
                {{--</div>--}}
                {{--<div class="col-4">--}}
                {{--<p>Показы</p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="card-body">--}}
                {{--<div class="row">--}}
                {{--<div class="col-4">--}}
                {{--<div class="card-col__row">--}}
                {{--<div class="card-col__img">--}}
                {{--<img src="{{ asset('img/human-small.png') }}" alt="">--}}
                {{--</div>--}}
                {{--<div>--}}
                {{--<p>Характер</p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-2">--}}
                {{--<p>0,40 RUB</p>--}}
                {{--</div>--}}
                {{--<div class="col-2">--}}
                {{--<p>3</p>--}}
                {{--</div>--}}
                {{--<div class="col-4">--}}
                {{--<p>-</p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="row">--}}
                {{--<div class="col-4">--}}
                {{--<div class="card-col__row">--}}
                {{--<div class="card-col__img">--}}
                {{--<img src="{{ asset('img/human-small.png') }}" alt="">--}}
                {{--</div>--}}
                {{--<div>--}}
                {{--<p>Характер</p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-2">--}}
                {{--<p>0,40 RUB</p>--}}
                {{--</div>--}}
                {{--<div class="col-2">--}}
                {{--<p>3</p>--}}
                {{--</div>--}}
                {{--<div class="col-4">--}}
                {{--<p>-</p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="row">--}}
                {{--<div class="col-4">--}}
                {{--<div class="card-col__row">--}}
                {{--<div class="card-col__img">--}}
                {{--<img src="{{ asset('img/human-small.png') }}" alt="">--}}
                {{--</div>--}}
                {{--<div>--}}
                {{--<p>Характер</p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-2">--}}
                {{--<p>0,40 RUB</p>--}}
                {{--</div>--}}
                {{--<div class="col-2">--}}
                {{--<p>3</p>--}}
                {{--</div>--}}
                {{--<div class="col-4">--}}
                {{--<p>-</p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="row">--}}
                {{--<div class="col-4">--}}
                {{--<div class="card-col__row">--}}
                {{--<div class="card-col__img">--}}
                {{--<img src="{{ asset('img/human-small.png') }}" alt="">--}}
                {{--</div>--}}
                {{--<div>--}}
                {{--<p>Характер</p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-2">--}}
                {{--<p>0,40 RUB</p>--}}
                {{--</div>--}}
                {{--<div class="col-2">--}}
                {{--<p>3</p>--}}
                {{--</div>--}}
                {{--<div class="col-4">--}}
                {{--<p>-</p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="row">--}}
                {{--<div class="col-4">--}}
                {{--<div class="card-col__row">--}}
                {{--<div class="card-col__img">--}}
                {{--<img src="{{ asset('img/human-small.png') }}" alt="">--}}
                {{--</div>--}}
                {{--<div>--}}
                {{--<p>Характер</p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-2">--}}
                {{--<p>0,40 RUB</p>--}}
                {{--</div>--}}
                {{--<div class="col-2">--}}
                {{--<p>3</p>--}}
                {{--</div>--}}
                {{--<div class="col-4">--}}
                {{--<p>-</p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
    </section>
@stop