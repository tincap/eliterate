@extends('layouts.master')

@section('content')
    <section class="page-content statistics page-responsive">
        <div class="container">
            <div class="page-head">
                <div class="page-head__name">
                    <h1>Статистика</h1>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/">Главная</a></li>
                            <li class="breadcrumb-item active" aria-current="page">
                                <a href="{{ route('statistics') }}">Статистика</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <ul class="tabs">
                <li><a href="{{ route('statistics') }}">офферы</a></li>
                <li><a class="tabs-active" href="{{ route('statistics.video') }}">видео-кампании</a></li>
            </ul>
            <div class="form-row statistics-select">
                <select class="form-control">
                    <option selected>28.03.2018-04.04.218</option>
                    <option>123</option>
                    <option>123</option>
                    <option>123</option>
                    <option>123</option>
                </select>
                <select class="form-control">
                    <option selected>Площадки</option>
                    <option>Площадки1</option>
                    <option>Площадки2</option>
                    <option>Площадки3</option>
                    <option>Площадки4</option>
                </select>
            </div>
            <div class="responsive-table statistics-table">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-4">
                                <p>Площадка</p>
                            </div>
                            <div class="col-2">
                                <p>Заработок</p>
                            </div>
                            <div class="col-2">
                                <p>Просмотры</p>
                            </div>
                            <div class="col-4">
                                <p>Показы</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-4">
                                <div class="card-col__row">
                                    <div class="card-col__img">
                                        <img src="{{ asset('img/human-small.png') }}" alt="">
                                    </div>
                                    <div>
                                        <p>Характер</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-2">
                                <p>0,40 RUB</p>
                            </div>
                            <div class="col-2">
                                <p>3</p>
                            </div>
                            <div class="col-4">
                                <p>-</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <div class="card-col__row">
                                    <div class="card-col__img">
                                        <img src="{{ asset('img/human-small.png') }}" alt="">
                                    </div>
                                    <div>
                                        <p>Характер</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-2">
                                <p>0,40 RUB</p>
                            </div>
                            <div class="col-2">
                                <p>3</p>
                            </div>
                            <div class="col-4">
                                <p>-</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <div class="card-col__row">
                                    <div class="card-col__img">
                                        <img src="{{ asset('img/human-small.png') }}" alt="">
                                    </div>
                                    <div>
                                        <p>Характер</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-2">
                                <p>0,40 RUB</p>
                            </div>
                            <div class="col-2">
                                <p>3</p>
                            </div>
                            <div class="col-4">
                                <p>-</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <div class="card-col__row">
                                    <div class="card-col__img">
                                        <img src="{{ asset('img/human-small.png') }}" alt="">
                                    </div>
                                    <div>
                                        <p>Характер</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-2">
                                <p>0,40 RUB</p>
                            </div>
                            <div class="col-2">
                                <p>3</p>
                            </div>
                            <div class="col-4">
                                <p>-</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <div class="card-col__row">
                                    <div class="card-col__img">
                                        <img src="{{ asset('img/human-small.png') }}" alt="">
                                    </div>
                                    <div>
                                        <p>Характер</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-2">
                                <p>0,40 RUB</p>
                            </div>
                            <div class="col-2">
                                <p>3</p>
                            </div>
                            <div class="col-4">
                                <p>-</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop