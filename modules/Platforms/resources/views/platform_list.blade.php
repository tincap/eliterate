@foreach($platforms as $platform)
    <div class="row">
        <div class="col-4">
            <div class="card-col__row">
                <div class="card-col__img">
                    <img src="img/human-small.png" alt="">
                </div>
                <div>
                    <p>{{ $platform->name }}</p>
                </div>
            </div>
        </div>
        <div class="col-5">
            <p>{{ $platform->profile }}</p>
        </div>
        <div class="col-3 button-right">
            <a class="btn btn-outline-secondary" href="#">Сменить аккаунт</a>
        </div>
    </div>
@endforeach