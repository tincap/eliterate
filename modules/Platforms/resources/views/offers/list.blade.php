@forelse($offers as $offer)
    <div class="row">
        <div class="col-4">
            <div class="card-col__first">
                <div class="card-col__img">
                    <img src="{{ $offer->image_link }}" alt="">
                </div>
                <div>
                    <p>{{ $offer->name }}</p>
                    <a class="btn btn-success" href="{{ route('offers.show', ['slug' => $offer->slug]) }}">
                        Подключить
                    </a>
                </div>
            </div>
        </div>
        <div class="col-2">
            <p>Подтвержденная заявка</p>
        </div>
        <div class="col-2">
            <ul>
                @foreach ($offer->rates as $rate)
                    <li>{{ $rate->country->name }}</li>
                @endforeach
            </ul>
        </div>
        <div class="col-2">
            <ul>
                @foreach ($offer->rates as $rate)
                    <li>{{ $rate->price }} {{ $rate->country->currency }}</li>
                @endforeach
            </ul>
        </div>
        <div class="col-2">
            <ul>
                @foreach ($offer->rates as $rate)
                    <li>{{ $rate->rate }} {{ $rate->country->currency }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@empty
    <div class="">По данному запросу ничего не найдено</div>
@endforelse