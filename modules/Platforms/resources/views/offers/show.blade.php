@extends('layouts.master')

@section('content')
    <section class="page-content view-offer page-responsive">
        <div class="container">
            <div class="page-head">
                <div class="page-head__name">
                    <h1>{{ $offer->name }}</h1>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Главная</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('offers.index') }}">Офферы</a></li>
                            <li class="breadcrumb-item active" aria-current="page">
                                <a href="{{ route('offers.show', ['slug' => $offer->slug]) }}">Женское нижнее белье</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="row view-offer">
                <div class="col-md-8">
                    <div class="view-offer__info">
                        <div class="view-offer__row">
                            <div class="view-offer__img">
                                <img src="{{ asset($offer->image_link) }}" alt="">
                            </div>
                            <div class="view-offer__descr">
                                <p><a href="{{ $offer->link }}">{{ $offer->link }}</a></p>
                                <a href="{{ route('stream.create', ['id' => $offer->id]) }}" class="btn btn-success">
                                    Создать поток
                                </a>
                                <a href="#" class="btn btn-outline-danger">Убрать из моих офферов</a>
                            </div>
                        </div>
                        <div class="view-offer__col">
                            <span>Краткое описание</span>
                            <p>{!! $offer->description !!}</p>
                        </div>
                        <div class="view-offer__col">
                            <span>Правила оффера</span>
                            <p>{!! $offer->rules !!}</p>
                        </div>
                        <div class="view-offer__bottom">
                            <div>
                                <span>Цели</span>
                                <p>Подтвержденная заявка</p>
                            </div>
                            <div>
                                <span>Постклик</span>
                                <p>15 дней</p>
                            </div>
                        </div>
                    </div>
                    <div class="company-table">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-4">
                                        <p>Страны</p>
                                    </div>
                                    <div class="col-4">
                                        <p>Цена</p>
                                </div>
                                    <div class="col-2">
                                        <p>Выплата</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body card-playgrounds">
                                @foreach( $offer->rates as $rate)
                                <div class="row">
                                    <div class="col-4">
                                        <p>
                                            <img src="{{ asset($rate->country->img_url)}}" alt="">
                                            <span>{{ $rate->country->name }}</span>
                                        </p>
                                    </div>
                                    <div class="col-4">
                                        <p>{{ $rate->price }} {{$rate->country->currency }}</p>
                                    </div>
                                    <div class="col-2">
                                        <p>{{ $rate->rate }} {{$rate->country->currency }}</p>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="news-block">
                        <h2>Новости</h2>
                        @foreach ($news as $article)
                            <div class="news-block__row">
                                <div class="news-block__img">
                                    <img src="{{ asset($article->image_url) }}" alt="">
                                </div>
                                <div class="news-block__descr">
                                    <h3><a href="{{ route('news.show', ['id' => $article->id]) }}">{{ $article->name }}</a></h3>
                                    <span>{{ $article->created_at->format('d:m:Y H:i') }}</span>
                                    <p>{{ $article->text }}</p>
                                </div>
                            </div>
                        @endforeach

                        <div class="news-block__link">
                            <a href="{{ route('news.index') }}">Все новости оффера</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="sidebar-offer">
                        <h3>Показатели конверсии</h3>
                        <div>
                            <p>CP: 13%</p>
                            <p>EPC: 1%</p>
                        </div>
                        <h3>Типы трафика</h3>
                        <ul>
                            @foreach ($traffics as $traffic)
                                <li>
                                    <i class="fa {{ $offer->traffics->contains($traffic->id) ? 'fa-check' : 'fa-times' }}" aria-hidden="true"></i>
                                    <span>{{ $traffic->name }}</span>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop