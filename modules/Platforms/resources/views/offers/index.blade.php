@extends('layouts.master')

@section('content')
    <section class="page-content offer page-responsive">
        <div class="container">
            <div class="page-head">
                <div class="page-head__name">
                    <h1>Офферы</h1>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Главная</a></li>
                            <li class="breadcrumb-item active" aria-current="page">
                                <a href="{{ route('offers.index') }}">Офферы</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="page-head__right">
                    <div class="drop">
                        <div>
                            <p>Все страны</p>
                            <ul>
                                @foreach($countries as $country)
                                    <li><a href="#"><img src="{{ asset($country->imgUrl) }}" alt=""></a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="drop">
                        <div>
                            <p>Все валюты</p>
                            <ul>
                                <li><a href="#"><img src="{{ asset('vendors/flags/1x1/hn.svg') }}" alt=""></a></li>
                                <li><a href="#"><img src="{{ asset('vendors/flags/1x1/hn.svg') }}" alt=""></a></li>
                                <li><a href="#"><img src="{{ asset('vendors/flags/1x1/hn.svg') }}" alt=""></a></li>
                                <li><a href="#"><img src="{{ asset('vendors/flags/1x1/hn.svg') }}" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="search-input">
                        <input type="text" placeholder="Поиск">
                        <i class="fa fa-search" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
            <ul class="tabs">
                <li><a class="{{ $activeCategory === 'all' ? 'tabs-active' : '' }}" href="{{ route('offers.index') }}">
                        Все
                    </a>
                </li>
                @foreach($categories as $category)
                    <li><a class="{{ $activeCategory === $category->slug ? 'tabs-active' : '' }}" href="{{ route('offers.index', ['category' => $category->slug]) }}">
                            {{ $category->name }}
                        </a>
                    </li>
                @endforeach
            </ul>
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-4">
                            <p>Цена</p>
                        </div>
                        <div class="col-2">
                            <p>Цели</p>
                        </div>
                        <div class="col-2">
                            <p>Страны</p>
                        </div>
                        <div class="col-2">
                            <p>Цена</p>
                        </div>
                        <div class="col-2">
                            <p>Выплата</p>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    @include('platforms::offers.list')
                </div>
            </div>
        </div>
    </section>

    <script type="text/javascript">
        window.onload = function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.search-input input').on('input', function () {
                $.ajax({
                    type: "GET",
                    data: {
                        search: $(this).val()
                    },
                    success: function (response) {
                        $('.card-body').html(response)
                    },
                });
            })
        }
    </script>
@stop