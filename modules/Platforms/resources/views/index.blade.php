@extends('layouts.master')

@section('content')
    <section class="page-content playgrounds">
        <div class="container">
            <div class="page-head">
                <div class="page-head__name">
                    <h1>Мои площадки</h1>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/">Главная</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><a href="#">Мои площадки</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="page-head__right">
                    <div class="drop">
                        <div>
                            <p>Все валюты</p>
                            <ul>
                                <li><a href="#"><img src="vendors/flags/1x1/hn.svg" alt=""></a></li>
                                <li><a href="#"><img src="vendors/flags/1x1/hn.svg" alt=""></a></li>
                                <li><a href="#"><img src="vendors/flags/1x1/hn.svg" alt=""></a></li>
                                <li><a href="#"><img src="vendors/flags/1x1/hn.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="search-input">
                        <input type="text" placeholder="Поиск">
                        <i class="fa fa-search" aria-hidden="true"></i>
                    </div>
                    <div class="button-add">
                        <a href="{{ route('user.platforms.store') }}">+ Добавить площадку</a>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-4">
                            <p>Площадка</p>
                        </div>
                        <div class="col-5">
                            <p>Профиль для публикаций</p>
                        </div>
                    </div>
                </div>
                <div class="card-body card-playgrounds">
                    @include('platforms::platform_list')
                </div>
            </div>
        </div>
    </section>

    <script>
        window.onload = function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.search-input input').on('input', function () {
                $.ajax({
                    type: "POST",
                    data: {
                        search: $(this).val()
                    },
                    success: function (response) {
                        $('.card-playgrounds').html(response)
                    },
                });
            })
        }
    </script>
@stop
