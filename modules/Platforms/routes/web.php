<?php

Route::group(['middleware' => ['web', 'auth'], 'namespace' => 'Modules\Platforms\Http\Controllers'], function () {
    Route::get('/platform/create', 'PlatformsController@create')->name('user.platforms.store');
    Route::post('/platform/create', 'PlatformsController@store');

    Route::get('/platforms', 'PlatformsController@index')->name('user.platforms');
    Route::post('/platforms', 'PlatformsController@show');

    Route::group(['prefix' => '/offers', 'as' => 'offers.'], function () {
        Route::get('/', 'OfferController@index')->name('index');
        Route::get('/{offer}', 'OfferController@show')->name('show');
        Route::post('/', 'OfferController@search')->name('search');
    });

    Route::get('/statistics', 'StatisticsController@index')->name('statistics');
    Route::get('/statistics/video', 'StatisticsController@getVideoStatistics')->name('statistics.video');

    Route::get('/streams', 'StreamController@index')->name('streams');
    Route::get('/stream/{id}/delete', 'StreamController@destroy')->name('stream.delete');

    Route::get('/stream/{id}/stream', 'StreamController@create')->name('stream.create');
    Route::post('/stream/{id}/stream', 'StreamController@store');

    Route::get('/stream/{id}/edit', 'StreamController@edit')->name('stream.edit');
    Route::post('/stream/{id}/edit', 'StreamController@update');

    Route::post('/stream/create', 'StreamController@store');

    Route::resource('news', 'NewsController', [
        'only' => ['index', 'show']
    ])->parameters([
        'news' => 'article'
    ]);
});

Route::group(['namespace' => 'Modules\Platforms\Http\Controllers'], function () {
    Route::get('/link/{hash}/{subid?}', 'TrafficController@redirectUser')->name('redirectUser');


    Route::any('/postback/{system}', 'PostbackController@getPostback')->name('postback');
});
