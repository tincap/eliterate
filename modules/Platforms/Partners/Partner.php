<?php

namespace Modules\Platforms\Partners;


use Illuminate\Http\Request;

abstract class Partner
{

    /**
     * @return string
     */
    abstract static public function getName(): string;

    /**
     * @param string $url
     * @return int|null
     */
    abstract static public function getOfferIdByUrl(string $url): ?int;

    /**
     * @param int $id
     * @return \stdClass
     */
    abstract public function getOffer(int $id): \stdClass;

    /**
     * @return array
     */
    abstract public function getOffers(): array;

    /**
     * @param int $offer_id
     * @return array|null
     */
    abstract public function getLinksFromOffer(int $offer_id): ?array;

    /**
     * @param Request $request
     * @return mixed
     */
    abstract public function processPostBack(Request $request);

    /**
     * @return string
     */
    abstract public function getPostBackUrl(): string;
}