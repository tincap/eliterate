<?php
/**
 * Created by PhpStorm.
 * User: Artyom
 * Date: 30.04.2018
 * Time: 15:25
 */

namespace Modules\Platforms\Partners;


use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Modules\Platforms\Entities\Event;

class Foxyads extends Partner
{

    /**
     * @var string
     */
    private $api_url = 'https://api.foxyads.ru/v1';

    /**
     * @var string
     */
    private $api_key;

    /**
     * @var string
     */
    private $login;

    /**
     * @var resource
     */
    private $curl;

    /**
     * Foxyads constructor.
     */
    public function __construct()
    {
        $this->api_key = env('FOXYADS_KEY');
        $this->login   = env('FOXYADS_LOGIN');

        $this->curl = curl_init();
    }

    /**
     * Foxyads destructor.
     */
    public function __destruct()
    {
        curl_close($this->curl);
    }

    /**
     * @param int $id
     * @return \stdClass
     */
    public function getOffer(int $id): \stdClass
    {
        $response = $this->_execute("offer/{$id}");

        return $response;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getOffers(): array
    {
        $response = $this->_execute("offers");

        return $response;
    }

    /**
     * @param int $offer_id
     * @return array|null
     */
    public function getLinksFromOffer(int $offer_id): ?array
    {
        // TODO: Implement getLinksFromOffer() method.
    }

    /**
     * @param string $action
     * @param string $method
     * @return mixed
     * @throws \Exception
     */
    private function _execute(string $action, string $method = 'GET')
    {
        $auth_header = base64_encode("{$this->login}:{$this->api_key}");

        curl_setopt_array($this->curl, array(
            CURLOPT_URL            => "{$this->api_url}/{$action}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => $method,
            CURLOPT_HTTPHEADER     => array(
                "Authorization: Basic {$auth_header}"
            ),
        ));

        $response = json_decode(curl_exec($this->curl));
        $err      = curl_error($this->curl);

        if ($err) {
            throw new \Exception($err);
        }

        return $response;
    }

    /**
     * @param string $url
     * @return int|null
     */
    static public function getOfferIdByUrl(string $url): ?int
    {
        return (int)preg_replace('/\D/', '', $url);
    }

    /**
     * @return string
     */
    static public function getName(): string
    {
        return 'FOXYADS';
    }

    /**
     * POST
     *
     * @param Request $request
     * @return mixed
     */
    public function processPostBack(Request $request)
    {
        $event = Event::firstOrNew([
            'order_id' => $request->get('uniqueid'),
            'partner'  => PartnerPrograms::PARTNER_FOXYADS,
        ]);

        $event->fill([
            'offer_id'   => $request->get('offer'),
            'stream_id'  => $request->get('stream'),
            'ip'         => $request->get('ip'),
            'target'     => Event::TARGET_POSTBACK,
            'status'     => $this->getStatus($request->get('status')),
            'country'    => Event::COUNTRY_UNDEFINED,
            'log'        => json_encode($request->toArray()),
            'created_at' => Carbon::createFromTimestamp($request->get('time')),
        ]);

        $event->save(['timestamp' => false]);

        return;
    }

    /**
     * @param string $status
     * @return string
     */
    protected function getStatus(string $status): string
    {
        if ($status == 'approved') {
            return Event::STATUS_SUCCESS;
        }

        if ($status == 'hold' || $status == 'wait') {
            return Event::STATUS_WAITING;
        }

        if ($status == 'rejected') {
            return Event::STATUS_FAILED;
        }

        return Event::STATUS_UNDEFINED;
    }

    /**
     * @return string
     */
    public function getPostBackUrl(): string
    {
        return route('postback', [
                'system' => PartnerPrograms::PARTNER_FOXYADS,
            ]).'?uniqueid={uniqueid}&status={status}&offer={offer}&stream={stream}&landing={landing}&subid1={subid1}&ip={ip}&time={time}';
    }
}