<?php

namespace Modules\Platforms\Socials;

use Modules\Users\Entities\User;
use VK\VK;
use VK\VKException;

class SuperVK extends VK
{

    /**
     * @var User|null
     */
    private $user;
    private $social;

    /**
     * SuperVK constructor.
     * @param User|null $user
     * @throws \VK\VKException
     */
    public function __construct(User $user = null)
    {
        $access_token = null;

        if ($user) {
            $this->social = $user->socials()->where('type', 'vk')->first();
            $this->user   = $user;

            if ($user && $this->social) {
                $access_token = $this->social->token;
            }
        }

        parent::__construct(env('VK_ID'), env('VK_SECRET'), $access_token);
    }

    /**
     * @param $redirect_url
     * @return string
     */
    public function getLoginUrl($redirect_url)
    {
        return $this->getAuthorizeUrl('offline,video,groups,photos,wall', $redirect_url);
    }

    /**
     * @param $url
     */
    public function publishVideo($url)
    {
        $response = $this->api('video.save', [
            'link'     => $url,
            'wallpost' => '1',
            'version'  => '5.74'
        ]);

        file_get_contents($response['response']['upload_url']);
//
//        $response = $vk->api('wall.post', [
//            'attachments' => 'video'.$groups['response']['owner_id'].'_'.$groups['response']['vid'],
//            'version'  => '5.74'
//        ]);

        return $response;
    }

    /**
     * @param string $code
     * @param string $callback_url
     * @return array
     * @throws VKException
     */
    public function getAccessToken($code, $callback_url = 'https://api.vk.com/blank.html')
    {
        if ($this->isAuth()) {
            throw new VKException('Already authorized.');
        }

        $rs = (array)json_decode(file_get_contents('https://api.vk.com/oauth/token?client_id='.env('VK_ID').
            '&code='.$code.'&client_secret='.env('VK_SECRET').'&redirect_uri='.$callback_url));

        if (isset($rs['error'])) {
            throw new VKException($rs['error'].
                (! isset($rs['error_description']) ?: ': '.$rs['error_description']));
        } else {
            $this->setAccessToken($rs['access_token']);
            $this->isAuth();

            return $rs;
        }
    }
}