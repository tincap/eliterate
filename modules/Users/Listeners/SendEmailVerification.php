<?php

namespace Modules\Users\Listeners;

use Illuminate\Auth\Events\Registered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Users\Entities\User;
use Modules\Users\Notifications\EmailConfirmationNotification;

class SendEmailVerification
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param Registered $event
     * @return void
     */
    public function handle(Registered $event)
    {
        $token = $this->generateToken();

        $event->user->verification_token = $token;

        $event->user->save();

        $event->user->notify(new EmailConfirmationNotification($token));
    }

    /**
     * @return null|string
     */
    public function generateToken()
    {
        $token = null;

        while (! $token) {
            $tmp = uniqid();

            $user = User::where('verification_token', $tmp)->first();

            if (! $user) {
                $token = $tmp;
            }
        }

        return $token;
    }
}
