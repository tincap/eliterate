<?php
/**
 * Created by PhpStorm.
 * User: Artyom
 * Date: 16.05.2018
 * Time: 12:47
 */

namespace Modules\Users\Transactions;


use Illuminate\Http\Request;
use Modules\Users\Entities\Transaction;
use Modules\Users\Entities\User;
use Modules\Users\Entities\Wallet;

class Robokassa implements PaymentInterface
{

    protected $payment_description  = 'Пополнение баланса на сервисе Eliterate';
    protected $transfer_description = 'Вывод средств с сервиса Eliterate';

    private $curl;

    public function __construct()
    {
        $this->curl = curl_init();
    }

    public function __destruct()
    {
        curl_close($this->curl);
    }

    /**
     * Generate HTML Form and create Transaction
     *
     * @param int $amount
     * @param User $user
     * @return string
     */
    public function getForm(int $amount, User $user): string
    {
        $transaction = $user->transactions()->create([
            'amount'  => $amount,
            'balance' => $user->balance,
            'system'  => PaymentService::SERVICE_ROBOKASSA,
            'type'    => Transaction::TYPE_INPUT,
            'status'  => Transaction::STATUS_WAITING,
        ]);
        $desc        = $this->payment_description;
        $sign        = $this->getHash($amount, $transaction->id);

        return view('users::payments.templates.robokassa',
            compact('transaction', 'desc', 'sign'))->render();
    }

    public function getHash($outSum, $invId, $mode = false)
    {
        $login = env('ROBOKASSA_LOGIN');
        $pass1 = env('ROBOKASSA_PASS1');
        $pass2 = env('ROBOKASSA_PASS2');

        return $mode ?
            md5("{$login}:{$invId}:{$pass2}") :
            md5("{$login}:{$outSum}:{$invId}:{$pass1}");
    }

    /**
     * Validate Callback Transaction
     *
     * @param Request $request
     * @return bool
     */
    public function verifyPayment(Request $request): bool
    {
        $transaction = Transaction::find($request->get('InvId', 0));

        if ($transaction) {
            $hash = $this->getHash($transaction->amount, $transaction->id);

            return hash_equals($hash, $request->get('SignatureValue', ''));
        }

        return false;
    }

    /**
     * Return Transaction status from Payment system
     *
     * @param Transaction $transaction
     * @return string
     */
    public function getTransactionStatus(Transaction $transaction): string
    {
        $data = http_build_query([
            'MerchantLogin' => env('ROBOKASSA_LOGIN'),
            'InvoiceID'     => $transaction->id,
            'Signature'     => $this->getHash($transaction->amount, $transaction->id, true),
        ]);

        curl_setopt_array($this->curl, array(
            CURLOPT_URL            => "https://auth.robokassa.ru/Merchant/WebService/Service.asmx/OpState",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "POST",
            CURLOPT_POSTFIELDS     => $data,
            CURLOPT_HTTPHEADER     => array(
                "Cache-Control: no-cache"
            ),
        ));

        try {
            $response = curl_exec($this->curl);
            $err      = curl_error($this->curl);

            $transaction->update(['log' => $response]);

            $response = simplexml_load_string($response);

            if ($err) {
                \Log::error('Robokassa transaction status error: '.$err);
                return Transaction::STATUS_FAILED_LOAD;
            } else {
                if ($response->Result->Code === 100) {
                    return Transaction::STATUS_PAID;
                }

                if ($response->Result->Code === 5 or $response->Result->Code === 50) {
                    return Transaction::STATUS_WAITING;
                }

                return Transaction::STATUS_FAILED;
            }
        } catch (\Exception $e) {
            \Log::error('Robokassa transaction status error: '.$e->getMessage());
            return Transaction::STATUS_FAILED_LOAD;
        }
    }

    /**
     * Return true if can get transaction status from payment service
     *
     * @return bool
     */
    public function canGetStatus(): bool
    {
        return false;
    }

    /**
     * Transfer funds from service to user wallet
     * return true if success and false if fail
     *
     * @param Wallet $wallet
     * @param int $amount
     * @return bool
     */
    public function transferFunds(Wallet $wallet, int $amount): bool
    {
        return false;
    }
}