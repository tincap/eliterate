<?php

namespace Modules\Users\Entities;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{

    protected $fillable = ['number'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }
}
