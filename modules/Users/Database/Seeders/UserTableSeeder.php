<?php

namespace Modules\Users\Database\Seeders;

use Faker\Generator;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Users\Entities\User;

class UserTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Generator $faker)
    {
        Model::unguard();

        User::truncate();

        User::create([
            'email'    => 'admin@admin.com',
            'name'     => 'admin',
            'password' => 'password',
            'balance'  => rand(1000, 9000),
            'role'     => 'ADMIN'
        ]);

        foreach (range(1, 10) as $i) {
            User::create([
                'email'    => $faker->unique()->email,
                'name'     => $faker->name,
                'password' => 'password',
                'phone'    => $faker->unique()->phoneNumber,
                'balance'  => rand(1000, 9000)
            ]);
        }
    }
}
