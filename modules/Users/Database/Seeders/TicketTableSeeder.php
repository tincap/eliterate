<?php

namespace Modules\Users\Database\Seeders;

use Faker\Generator;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Users\Entities\Message;
use Modules\Users\Entities\Ticket;
use Modules\Users\Entities\User;

class TicketTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Generator $faker)
    {
        Model::unguard();
        Ticket::truncate();
        Message::truncate();

        $statuses = collect(['open', 'closed']);

        foreach (range(0, 10) as $i) {
            $ticket = Ticket::create([
                'title'   => $faker->title,
                'message' => $faker->realText(),
                'user_id' => User::all()->random()->id,
                'status'  => $statuses->random()
            ]);

            foreach (range(0, rand(1, 3)) as $m) {
                $ticket->messages()->create([
                    'user_id' => User::all()->random()->id,
                    'message' => $faker->realText(),
                ]);
            }
        }
    }
}