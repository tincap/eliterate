<?php

namespace Modules\Users\Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class WalletsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('wallets')->insert([
            [
                'number' => 'WMR 111111111',
                'user_id' => 1
            ],
            [
                'number' => 'WMR 222222222',
                'user_id' => 1
            ],
            [
                'number' => 'WMR 333333333',
                'user_id' => 1
            ],
        ]);
    }
}
