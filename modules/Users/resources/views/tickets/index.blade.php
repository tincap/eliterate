@extends('layouts.master')

@section('content')
    <section class="page-content payments page-responsive">
        <div class="container">
            <div class="page-head">
                <div class="page-head__name">
                    <h1>Тикеты</h1>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Главная</a></li>
                            <li class="breadcrumb-item active" aria-current="page">
                                <a href="{{ route('tickets.index') }}">Тикеты</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="tab-content" id="myTabContent">
                <div id="card-finance" class="tab-pane fade show active" id="card-finance" role="tabpanel"
                     aria-labelledby="finance-tab">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-2">
                                    <p>№</p>
                                </div>
                                <div class="col-2">
                                    <p>Создан</p>
                                </div>
                                <div class="col-4">
                                    <p>Тема</p>
                                </div>
                                <div class="col-2">
                                    <p>Статус</p>
                                </div>
                                <div class="col-2">
                                    <p>Ответов</p>
                                </div>
                            </div>
                        </div>
                        <div class="card-body card-playgrounds">
                            @forelse($tickets as $ticket)
                                <div class="row">
                                    <div class="col-2">
                                        <p>{{ $ticket->id }}</p>
                                    </div>
                                    <div class="col-2">
                                        <p>{{ $ticket->created_at->format('d.m.Y в H:i') }}</p>
                                    </div>
                                    <div class="col-4">
                                        <p>
                                            <a href="{{ route('tickets.show', ['id' => $ticket->id]) }}">
                                                {{ $ticket->title }}
                                            </a>
                                        </p>
                                    </div>
                                    <div class="col-2">
                                        <p>{{ $ticket->status }}</p>
                                    </div>
                                    <div class="col-2">
                                        <p>{{ $ticket->messages->count() }}</p>
                                    </div>
                                </div>
                            @empty
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12" style="text-align: center">
                                            Вы ещё не создавали тикеты
                                        </div>
                                    </div>
                                </div>
                            @endforelse
                        </div>
                    </div>
                </div>
                <div id="card-payment" class="tab-pane fade" id="card-payment" role="tabpanel"
                     aria-labelledby="payment-tab">
                    <div class="form-row statistics-select payments-sort">
                        <form action="">
                            <select class="form-control">
                                <option selected>RUB</option>
                                <option>123</option>
                                <option>123</option>
                                <option>123</option>
                                <option>123</option>
                            </select>
                            <div class="form-inline">
                                <label for="44">Сумма выплаты</label>
                                <input type="text" class="form-control" id="44" placeholder="2 000.00">
                            </div>
                            <select class="form-control">
                                <option selected>Выплатить на ...</option>
                                <option>Площадки1</option>
                                <option>Площадки2</option>
                                <option>Площадки3</option>
                                <option>Площадки4</option>
                            </select>
                            <input class="btn btn-success button-submit" type="submit" value="Заказать выплату">
                        </form>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-3">
                                    <p>Дата</p>
                                </div>
                                <div class="col-2">
                                    <p>Сумма</p>
                                </div>
                                <div class="col-3">
                                    <p>Кошелек</p>
                                </div>
                                <div class="col-4">
                                    <p>Статус платежа</p>
                                </div>
                            </div>
                        </div>
                        <div class="card-body card-playgrounds">
                            <div class="row">
                                <div class="col-3">
                                    <p>03.04.2018 в 18:20</p>
                                </div>
                                <div class="col-2">
                                    <p>4 300 RUB</p>
                                </div>
                                <div class="col-3">
                                    <p>Webmoney WMR R16338628432</p>
                                </div>
                                <div class="col-4">
                                    <p class="color-green">Заявка обработана</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3">
                                    <p>03.04.2018 в 18:20</p>
                                </div>
                                <div class="col-2">
                                    <p>4 300 RUB</p>
                                </div>
                                <div class="col-3">
                                    <p>Webmoney WMR R16338628432</p>
                                </div>
                                <div class="col-4">
                                    <p class="color-green">Заявка обработана</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3">
                                    <p>03.04.2018 в 18:20</p>
                                </div>
                                <div class="col-2">
                                    <p>4 300 RUB</p>
                                </div>
                                <div class="col-3">
                                    <p>Webmoney WMR R16338628432</p>
                                </div>
                                <div class="col-4">
                                    <p class="color-green">Заявка обработана</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop