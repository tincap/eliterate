@extends('layouts.master')

@section('content')
    <section class="page-content ticket-chat page-responsive">
        <div class="container">
            <div class="page-head">
                <div class="page-head__name">
                    <h1>{{ $ticket->title }}</h1>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/">Главная</a></li>
                            <li class="breadcrumb-item" aria-current="page">
                                <a href="{{ route('tickets.index') }}">Тикеты</a>
                            </li>
                            <li class="breadcrumb-item active">{{ $ticket->title }}</li>
                        </ul>
                    </nav>
                </div>
            </div>

            <div class="adding-site__content">
                <div class="">
                    <div class="row">
                        <div class="col-2">
                            <img src="{{ asset('/img/human-small.png') }}" alt="">
                        </div>
                        <div class="col-10">
                            <div class="pull-left text-muted">Вы</div>
                            <div class="pull-right text-muted">{{ $ticket->created_at->format('d.m.Y в H:i') }}</div>
                            <div class="clear">
                                {{ $ticket->message }}
                            </div>
                        </div>
                    </div>

                    <hr>
                    @foreach($ticket->messages()->orderByDesc('created_at')->get() as $message)
                        <div class="row">
                            <div class="col-2">
                                <img src="{{ asset('/img/human-small.png') }}" alt="">
                            </div>
                            <div class="col-10">
                                <div class="pull-left text-muted">
                                    @if($message->user_id === \Auth::id())
                                        Вы
                                    @else
                                        {{ $message->user->name }}
                                    @endif
                                </div>
                                <div class="pull-right text-muted">{{ $message->created_at->format('d.m.Y в H:i') }}</div>
                                <div class="clear">
                                    {{ $message->message }}
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <hr>
                    <form method="post">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-2"></div>
                            <div class="col-10">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Сообщение</label>
                                    <textarea type="text" class="form-control" id="formGroupExampleInput"
                                              placeholder="Текст" name="message" required></textarea>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-success" href="">Добавить комментарий</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
    </section>
@stop