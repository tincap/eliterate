@foreach($user->payments as $payment)
    <div class="row">
        <div class="col-3">
            <p>{{ $payment->created_at->format('d.m.Y в h:i') }}</p>
        </div>
        <div class="col-2">
            <p>{{ $payment->balance }} RUB</p>
        </div>
        <div class="col-3">
            <p>{{ $payment->wallet->number }}</p>
        </div>
        <div class="col-4">
            <p class="color-{{ $payment->status === 'PAID' ? 'green' : 'red' }}">
                {{ $payment->getPaymentStatus() }}
            </p>
        </div>
    </div>
@endforeach