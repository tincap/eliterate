@component('mail::message')
Здравствуйте, {{ $user->name }} !

Благодарим вас за то, что вы стали пользователем eliterate!

Перейдите по ссылке ниже для подтверждения вашего email.

@component('mail::button', ['url' => route('user.profile', ['token' => $token])])
    Подтвердить
@endcomponent

С уважением,<br>
Команда {{ config('app.name') }}
@endcomponent