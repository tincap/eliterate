@extends('layouts.master')

@section('content')
    <section class="page-content settings">
        <div class="container">
            <div class="page-head">
                <div class="page-head__name">
                    <h1>Настройки</h1>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Главная</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><a
                                        href="{{ route('user.settings') }}">Настройки</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header title-dark">
                            <p>Изменить пароль</p>
                        </div>
                        <div class="card-body">
                            <form method="POST">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Старый пароль</label>
                                    <input type="password" class="form-control" id="formGroupExampleInput"
                                           name="old_password">
                                    @if ($errors->has('old_password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('old_password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="formGroupExampleInput2">Новый пароль</label>
                                    <input type="password" class="form-control" id="formGroupExampleInput2"
                                           name="password">
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="formGroupExampleInput2">Еще раз Новый пароль</label>
                                    <input type="password" class="form-control" id="formGroupExampleInput3"
                                           name="password_confirmation">
                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <input class="btn btn-success button-submit" type="submit" value="Сохранить">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header title-dark">
                            <p>Привязать аккаунт</p>
                        </div>
                        <div class="card-body">
                            <p class="card-text">Привяжите свой аккаунт из соцсети</p>
                            <div class="card-social">
                                <a href="{{ $vk_link }}">
                                    <i class="fa fa-vk" aria-hidden="true"></i><span>привязать</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection