<?php

use Illuminate\Support\Facades\Auth;

Route::group(['middleware' => 'web', 'namespace' => 'Modules\Users\Http\Controllers'], function () {
    Auth::routes();

    Route::post('image-upload', 'UploadImageController@store')->name('image_upload');

    Route::group(['middleware' => 'auth'], function () {
        Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

        Route::get('/home', 'HomeController@getHomePage')->name('home');
        Route::post('/home', 'HomeController@postEditUser')->name('home.post');

        Route::group(['prefix' => 'user', 'as' => 'user.'], function () {
            // Edit profile
            Route::get('/profile', 'UsersController@getProfilePage')->name('profile');
            Route::post('/profile', 'UsersController@postProfile');
            Route::get('/profile/verify-email', 'UsersController@verifyEmail')->name('verify');
            // Edit settings
            Route::get('/settings', 'UsersController@getSettingsPage')->name('settings');
            Route::post('/settings', 'UsersController@postSettings');
        });

        Route::group(['prefix' => '/wallets', 'as' => 'wallets.'], function () {
            Route::get('/create', 'WalletController@create')->name('create');
            Route::post('/create', 'WalletController@store');
        });

        Route::group(['prefix' => '/payments', 'as' => 'payments.'], function () {
            Route::get('/', 'PaymentController@index')->name('index');
            Route::post('/', 'PaymentController@store');
        });

        Route::group(['prefix' => '/tickets', 'as' => 'tickets.'], function () {
            Route::get('/', 'TicketController@index')->name('index');

            Route::get('/create', 'TicketController@create')->name('create');
            Route::post('/create', 'TicketController@store');

            Route::get('/{id}', 'TicketController@show')->name('show');
            Route::post('/{id}', 'TicketController@update');
        });

        Route::group(['prefix' => '/transactions', 'as' => 'transactions.'], function () {
            Route::get('/success', function () {
                return 'Success';
            })->name('success');
            Route::get('/fail', function () {
                return 'fail';
            })->name('fail');
        });
    });
});
