<?php
/**
 * Created by PhpStorm.
 * User: theardent
 * Date: 20.04.18
 * Time: 12:06
 */

namespace Modules\Users\Http\Controllers;

use Debugbar;
use Illuminate\Http\Request;
use Storage;

class UploadImageController
{

    public function store(Request $request)
    {
        Debugbar::disable();

        $file = $request->file('upload');

        $filename = str_random(4).'_'.$file->getClientOriginalName();
        Storage::putFileAs('public/upload/images/', $file, $filename);
        $url = asset(Storage::url('upload/images/'.$filename));

        return [
            'uploaded' => 1,
            'filename' => $filename,
            'url'      => $url,
        ];
    }
}