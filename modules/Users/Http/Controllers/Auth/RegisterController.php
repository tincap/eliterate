<?php

namespace Modules\Users\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Validator;
use Modules\Users\Entities\User;

class RegisterController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    public function redirectPath()
    {
        return auth()->user()->role === 'ADMIN' ? '/admin' : '/';
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $genders = User::GENDER;
        $mode    = request()->query('mode') === 'admin' ? 'admin' : 'users';
        return view("$mode::auth.register", compact('genders'));
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'                  => 'required|string|max:255',
            'email'                 => 'required|string|email|max:255|unique:users',
            'firstname'             => 'required|string|max:255',
            'lastname'              => 'required|string|max:255',
            'terms'                 => 'required|accepted',
            'password'              => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required|string|min:6|same:password',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     *
     * @return User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name'          => $data['name'],
            'email'         => $data['email'],
            'password'      => $data['password'],
            'firstname'     => $data['firstname'],
            'lastname'      => $data['lastname'],
            'notifications' => isset($data['notifications']) ? 1 : 0,
        ]);

        if (isset($data['gender'])) {
            $user->update(['gender' => $data['gender']]);
        }

        return $user;
    }
}
