$(document).ready(function(){

  $('.datepicker').datepicker();

  $('.drop p').on('click', function(){
    $(this).parent().parent().toggleClass('drop-open');
  });

  $('.top-bar__mob').on('click', function(){
    $('.menu .container').slideToggle(400);
  });

  $(document).click(function (e) {
    var elem = $(".drop");
    if (e.target != elem[0] && !elem.has(e.target).length){
        elem.removeClass('drop-open');
    }
	});

  $(window).resize(function(){
    if($(this).width() > 860)
      $('.menu .container').css('display', 'block');
    else
      $('.menu .container').css('display', 'none');
  });

  $('[data-toggle="tooltip"]').tooltip();
  $('.carousel').carousel();

  $('#offisoff').change(function(){
    if($('#offisoff :selected').text() == 'Перевести по ссылке')
      $('.card-show').removeClass('card-hide');
    else
      $('.card-show').addClass('card-hide');
  });

  $('#myTab a').on('click', function (e) {
    e.preventDefault()
    $(this).tab('show')
  })

});