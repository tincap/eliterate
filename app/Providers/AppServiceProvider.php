<?php

namespace App\Providers;

use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        $global_menu = [
            'user.platforms'  => [
                'icon' => 'icon-wallet',
                'text' => 'Мои площадки',
            ],
            'campaigns.index' => [
                'icon' => 'icon-wallet',
                'text' => 'Видео-кампании',
            ],
            'offers.index'    => [
                'icon' => 'fa fa-star-o',
                'text' => 'Офферы',
            ],
            'statistics'      => [
                'icon' => 'icon-wallet',
                'text' => 'Статистика',
            ],
            'streams'         => [
                'icon' => 'icon-wallet',
                'text' => 'Потоки',
            ],
            'news.index'      => [
                'icon' => 'icon-support',
                'text' => 'Новости',
            ],
            'payments.index'  => [
                'icon' => 'icon-wallet',
                'text' => 'Выплаты',
            ],
        ];

        \View::share('global_menu', $global_menu);

        \Blade::if ('guest', function () {
            return auth()->guest();
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(IdeHelperServiceProvider::class);
        }
    }
}
