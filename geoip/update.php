<?php

file_put_contents('dat/GeoIP.dat.gz', file_get_contents('http://geolite.maxmind.com/download/geoip/database/GeoLiteCountry/GeoIP.dat.gz'));
file_put_contents('dat/GeoIPv6.dat.gz', file_get_contents('http://geolite.maxmind.com/download/geoip/database/GeoIPv6.dat.gz'));
file_put_contents('dat/GeoLiteCity.dat.gz', file_get_contents('http://geolite.maxmind.com/download/geoip/database/GeoLiteCity.dat.gz'));
file_put_contents('dat/GeoLiteCityv6.dat.gz', file_get_contents('http://geolite.maxmind.com/download/geoip/database/GeoLiteCityv6-beta/GeoLiteCityv6.dat.gz'));
file_put_contents('dat/GeoIPASNum.dat.gz', file_get_contents('http://download.maxmind.com/download/geoip/database/asnum/GeoIPASNum.dat.gz'));
file_put_contents('dat/GeoIPASNumv6.dat.gz', file_get_contents('http://download.maxmind.com/download/geoip/database/asnum/GeoIPASNumv6.dat.gz'));


function ungzip($file_name){
	$buffer_size = 4096;
	$out_file_name = str_replace(array('.gz'), array(''), $file_name);
	$file = gzopen($file_name, 'rb');
	$out_file = fopen($out_file_name, 'wb'); 
	while (!gzeof($file)) {
		fwrite($out_file, gzread($file, $buffer_size));
	}
	fclose($out_file);
	gzclose($file);

}

ungzip('dat/GeoIP.dat.gz');
ungzip('dat/GeoIPv6.dat.gz');
ungzip('dat/GeoLiteCity.dat.gz');
ungzip('dat/GeoLiteCityv6.dat.gz');
ungzip('dat/GeoIPASNum.dat.gz');
ungzip('dat/GeoIPASNumv6.dat.gz');

unlink('GeoIP.dat');
unlink('GeoIPv6.dat');
unlink('GeoLiteCity.dat');
unlink('GeoLiteCityv6.dat');
unlink('GeoIPASNum.dat');
unlink('GeoIPASNumv6.dat');

unlink('dat/GeoIP.dat.gz');
unlink('dat/GeoIPv6.dat.gz');
unlink('dat/GeoLiteCity.dat.gz');
unlink('dat/GeoLiteCityv6.dat.gz');
unlink('dat/GeoIPASNum.dat.gz');
unlink('dat/GeoIPASNumv6.dat.gz');

var_dump(copy('/app/geoip/dat/GeoIP.dat', '/app/geoip/GeoIP.dat'));
var_dump(copy('/app/geoip/dat/GeoIPv6.dat', '/app/geoip/GeoIPv6.dat'));
var_dump(copy('/app/geoip/dat/GeoLiteCity.dat', '/app/geoip/GeoLiteCity.dat'));
var_dump(copy('/app/geoip/dat/GeoLiteCityv6.dat', '/app/geoip/GeoLiteCityv6.dat'));
var_dump(copy('/app/geoip/dat/GeoIPASNum.dat', '/app/geoip/GeoIPASNum.dat'));
var_dump(copy('/app/geoip/dat/GeoIPASNumv6.dat', '/app/geoip/GeoIPASNumv6.dat'));

unlink('dat/GeoIP.dat');
unlink('dat/GeoIPv6.dat');
unlink('dat/GeoLiteCity.dat');
unlink('dat/GeoLiteCityv6.dat');
unlink('dat/GeoIPASNum.dat');
unlink('dat/GeoIPASNumv6.dat');

